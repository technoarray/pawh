import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput,Alert} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import CommonButton from '../common/CommonButton';
import SelectButton from '../common/SelectButton';
import Header from '../common/Headerwithback'
import { Icon } from 'react-native-elements'
import BottomTabs from '../common/BottomTabs'
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

import { Fonts } from '../../utils/Fonts';

import Swiper from 'react-native-swiper-animated';

var home=require('../../themes/Images/2.png')
var pet=require('../../themes/Images/1.png')
var service=require('../../themes/Images/3.png')
var more=require('../../themes/Images/4.png')
const property = require('../../themes/Images/property.jpg')

let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      home:true,
      service:false,
      more:false,
      pet:false,
      fav_list:[]
    },
    _that = this;
  }

  componentWillMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
          console.log(uid);
          _that.validationAndApiParameter(uid,'fav')
        }
    })
  }

  validationAndApiParameter(uid,apiKey) {
    if(apiKey=='fav'){
      var data={
        uid:uid,
      }
      console.log(data);
      this.postToApiCalling('POST', 'fav', Constant.URL_showFav, data);
    }
    if(apiKey == 'unlike'){
      var data={
        uid:this.state.uid,
        aid:uid
      }
      console.log(data);
      this.postToApiCalling('POST', 'unlike', Constant.URL_Unlike, data);
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {

     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
        console.log(jsonRes);
          if ((!jsonRes) || (jsonRes.code == 0)) {
            console.log(jsonRes);
            Alert.alert(
              'Alert',
              'No Favourite Found.',
              [
                {
                  text: 'Ok',
                  onPress: () => _that.props.navigation.navigate('HomeScreen'),
                  style: 'cancel',
                },
              ],
              {cancelable: false},
            );
            _that.setState({ isVisible: false })
          }else {
            if (jsonRes.code == 1) {
                _that.apiSuccessfullResponse(apiKey, jsonRes)
            }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(()=>{
              Alert.alert("Server issue" + error);
          },200);
      });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if(apiKey=='fav'){
      console.log(jsonRes);
      this.setState({fav_list:jsonRes.fav_list})
    }
    if(apiKey=='unlike'){
      console.log(jsonRes);
      _that.validationAndApiParameter(this.state.uid,'fav')
    }
  }

  Homebtn=()=>{
    this.setState({
      home:true,
      service:false,
      more:false,
      pet:false
    })
    _that.props.navigation.navigate('HomeScreen',{home:true,service:false,more:false,pet:false});
  }
  Servicebtn=()=>{
    this.setState({
      home:false,
      service:true,
      more:false,
      pet:false
    })
    _that.props.navigation.navigate('ServicesScreen',{home:false,service:true,more:false,pet:false});
  }

  Morebtn=()=>{
    this.setState({
      home:false,
      service:false,
      more:true,
      pet:false
    })
    _that.props.navigation.navigate('SettingScreen',{home:false,service:false,more:true,pet:false});
  }
  Petbtn=()=>{
    this.setState({
      home:false,
      service:false,
      more:false,
      pet:true
    })
    _that.props.navigation.navigate('Pet_detail_form',{home:false,service:false,more:false,pet:true});
  }

  Singlebtn=()=>{
    const resetAction = NavigationActions.navigate({ routeName: 'SingleScreen'})
    _that.props.navigation.dispatch(resetAction);
  }
  Addbtn=()=>{
    const resetAction = NavigationActions.navigate({ routeName: 'Add_adoption'})
    _that.props.navigation.dispatch(resetAction);
  }


  render() {
    const headerProp = {
      title: 'FAVOURITES',
      screens: '',
    };

    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>


            <ScrollView style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
                <View style={styles.mainbox}>
                  {this.state.fav_list.map((data)=>
                    <TouchableOpacity  style={[styles.box,styles.shadow]} key={data.id}>
                        <View style={styles.imgbox}>
                            <Image source={{uri: Constant.image_path+data.image}} style={styles.productimg}/>
                        </View>
                        <View style={styles.contentbox}>
                          <TouchableOpacity onPress={()=>_that.props.navigation.navigate('SingleScreen',{home:true,service:false,more:false,pet:false,pet_id:data.id})}>
                                <Text style={styles.price}>{data.name}</Text>
                          </TouchableOpacity>
                          <Text style={styles.gender}><Text style={styles.bold}>Gender</Text> - {data.gender}</Text>
                          <Text style={styles.breed}><Text style={styles.bold}>Breed</Text>- {data.pet_breed}</Text>
                          <Text style={styles.location}><Text style={styles.bold}>Location</Text>- {data.address}</Text>
                          <TouchableOpacity onPress={() => _that.validationAndApiParameter(data.id,'unlike')} style={styles.favicon}>
                              <Icon
                                raised
                                sizes={18}
                                name='heart'
                                type='font-awesome'
                                color='#c41a39'
                              />
                          </TouchableOpacity>

                        </View>
                    </TouchableOpacity>
                  )}
                </View>
            </ScrollView>
            {/*}<TouchableOpacity onPress={this.Addbtn} style={styles.plus}>
            <Icon
                name='plus-circle'
                type='font-awesome'
                size={50}
                color='#c41a39'
                />
            </TouchableOpacity>*/}
            <View style={{backgroundColor:'#c41a39',zIndex:9999,width:'100%',height:50,flexDirection:'row',justifyContent:'space-around',position:'absolute',bottom:0}}>
              <TouchableOpacity onPress={this.Homebtn}  style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.home?{opacity:1}:{opacity:0.5}]}>
                <Image source={home} style={styles.icon}/>
                <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>Adopt</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.Petbtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.pet?{opacity:1}:{opacity:0.5}]}>
                <Image source={pet} style={styles.icon}/>
                <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>My Pet</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.Servicebtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.service?{opacity:1}:{opacity:0.5}]}>
                <Image source={service} style={styles.icon}/>
                <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>Services</Text>
              </TouchableOpacity>
              <TouchableOpacity  onPress={this.Morebtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.more?{opacity:1}:{opacity:0.5}]}>
                <Image source={more} style={styles.icon}/>
                <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>More</Text>
              </TouchableOpacity>
            </View>
      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center',
    width:'100%',
    backgroundColor:'#fff'
  },
  content: {
      flex: 1,
      width:'100%',
      height:'100%',
      position:'relative'
  },
  box:{
    width:'90%',
    //backgroundColor:'green',
    alignItems:'center',
    marginBottom:15,
    flexDirection:'row',
    //height:AppSizes.ResponsiveSize.height/2
    padding:10,
    borderRadius:15,
    textAlign:'center',
    position:'relative'
  },
  icon1:{
    width:AppSizes.screen.width/20,
      height:AppSizes.screen.height/40,
  },

  price:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    marginBottom:AppSizes.ResponsiveSize.Padding(1),
    color:'#000',
    fontWeight:'700',
    fontFamily:'Roboto-Bold'
  },
  gender:{
    fontSize:AppSizes.ResponsiveSize.Sizes(10),
    color:'#333',
marginBottom:AppSizes.ResponsiveSize.Padding(.50),
  fontFamily:Fonts.Robotor
    },
    bold:{
fontWeight:'700',
fontFamily:'Roboto-Bold'
    },
    breed:{
      fontSize:AppSizes.ResponsiveSize.Sizes(10),
      color:'#333',
marginBottom:AppSizes.ResponsiveSize.Padding(.50),
  fontFamily:Fonts.Robotor
      },
      location:{
        fontSize:AppSizes.ResponsiveSize.Sizes(10),
        color:'#333',
  marginBottom:AppSizes.ResponsiveSize.Padding(.50),
    fontFamily:Fonts.Robotor
        },
  imgbox:{
    width:'30%',
    height:100,
  },
  contentbox:{
    width:'70%',
    paddingLeft:10
  },
  productimg:{
    width:'100%',
    height:'100%',
    borderRadius:5
  },
  mainbox:{
    width:'100%',
    alignItems:'center',
    paddingTop:AppSizes.ResponsiveSize.Padding(3),
    marginBottom:80
  },
  shadow:{
    borderWidth:0,
    borderRadius: 5,
    justifyContent:'center',
    backgroundColor:'#f2f2f2',
    borderColor: '#999',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.5,
    shadowRadius: 3,
    elevation: 2,
    zIndex:0,
 },
 favicon:{
   position:'absolute',
   right:10,
   top:0
 },
 plus:{
   position:'absolute',
   right:15,
   height:45,
   width:45,
   bottom:15,
 },
 icon:{
   width:28,
   height:28
 }

});
