import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import CommonButton from '../common/CommonButton';
import SelectButton from '../common/SelectButton';
import Header from '../common/Headerwithback'
import FlipToggle from 'react-native-flip-toggle-button';
//import Icon from 'react-native-vector-icons/FontAwesome';
import BottomTabs from '../common/BottomTabs'
import Swiper from 'react-native-swiper-animated';

import { Fonts } from '../../utils/Fonts';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

var home=require('../../themes/Images/2.png')
var pet=require('../../themes/Images/1.png')
var service=require('../../themes/Images/3.png')
var more=require('../../themes/Images/4.png')

let _that

var username
var email
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      home:this.props.navigation.state.params.home,
      service:this.props.navigation.state.params.service,
      more:this.props.navigation.state.params.more,
      pet:this.props.navigation.state.params.pet
    },
    _that = this;
  }

  componentWillMount() {
      AsyncStorage.getItem('UserData').then((data) => {
            var data = JSON.parse(data);
            console.log(data);
            username=data.username
            email=data.email
            this.setState({
              username:username,
              email:email
            })
        })
    }

  Homebtn=()=>{
    this.setState({
      home:true,
      service:false,
      more:false,
      pet:false
    })
    _that.props.navigation.navigate('HomeScreen',{home:true,service:false,more:false,pet:false});
  }
  Servicebtn=()=>{
    this.setState({
      home:false,
      service:true,
      more:false,
      pet:false
    })
    _that.props.navigation.navigate('ServicesScreen',{home:false,service:true,more:false,pet:false});
  }

  Morebtn=()=>{
    this.setState({
      home:false,
      service:false,
      more:true,
      pet:false
    })
    _that.props.navigation.navigate('SettingScreen',{home:false,service:false,more:true,pet:false});
  }
  Petbtn=()=>{
    this.setState({
      home:false,
      service:false,
      more:false,
      pet:true
    })
    _that.props.navigation.navigate('Pet_detail_form',{home:false,service:false,more:false,pet:true});
  }

  profileBtn=()=>{
    const resetAction = NavigationActions.navigate({ routeName: 'edit_profile'})
    _that.props.navigation.navigate('edit_profile',{home:false,service:false,more:true,pet:false});
  }

  ReportBtn=()=>{
    const resetAction = NavigationActions.navigate({ routeName: 'Report_stray'})
    _that.props.navigation.dispatch(resetAction);
  }
  FeedBtn=()=>{
    const resetAction = NavigationActions.navigate({ routeName: 'GiveFeedback'})
    _that.props.navigation.dispatch(resetAction);
  }
  logout=()=>{
    AsyncStorage.removeItem("loggedIn");
    AsyncStorage.removeItem("UserData");
    AsyncStorage.removeItem("Animal_data");
    _that.props.navigation.navigate('SigninScreen')
  }

  render() {
    const headerProp = {
      title: 'More',
      screens: '',
    };

    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>

        <View style={styles.content}>
            <View style={styles.heading}><Text style={styles.text}>My Profile</Text></View>
            <View style={styles.box}>
                  <Text style={styles.name}>{this.state.username}</Text>
                  <Text style={styles.text}>{this.state.email}</Text>
                  <TouchableOpacity onPress={this.profileBtn}>
                      <Text style={styles.Password}>Change Password</Text>
                  </TouchableOpacity>
            </View>
            <View style={styles.heading}><Text style={styles.text}>Other Features</Text></View>
            <View style={styles.box1}>

                  <TouchableOpacity onPress={this.ReportBtn} style={styles.reportbox}>
                      <Text style={styles.Report}>Report Strays</Text>
                  </TouchableOpacity>
            </View>
            <View style={styles.box1}>

                  <TouchableOpacity onPress={this.FeedBtn} style={styles.reportbox}>
                      <Text style={styles.Report}>Give Feedback</Text>
                  </TouchableOpacity>
            </View>

            <View style={styles.box1}>

                  <TouchableOpacity onPress={this.logout} style={styles.reportbox}>
                      <Text style={styles.Report}>Logout</Text>
                  </TouchableOpacity>
            </View>
        </View>

        <View style={{backgroundColor:'#c41a39',zIndex:9999,width:'100%',height:50,flexDirection:'row',justifyContent:'space-around',position:'absolute',bottom:0}}>
          <TouchableOpacity onPress={this.Homebtn}  style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.home?{opacity:1}:{opacity:0.5}]}>
            <Image source={home} style={styles.icon}/>
            <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>Adopt</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.Petbtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.pet?{opacity:1}:{opacity:0.5}]}>
            <Image source={pet} style={styles.icon}/>
            <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>My Pet</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.Servicebtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.service?{opacity:1}:{opacity:0.5}]}>
            <Image source={service} style={styles.icon}/>
            <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>Services</Text>
          </TouchableOpacity>
          <TouchableOpacity  onPress={this.Morebtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.more?{opacity:1}:{opacity:0.5}]}>
            <Image source={more} style={styles.icon}/>
            <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>More</Text>
          </TouchableOpacity>
        </View>

      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center',
    width:'100%',
    backgroundColor:'#ffffff'
  },
    content: {
        flex: 1,
        flexDirection: 'column',
        height:'100%',
        //paddingTop:AppSizes.ResponsiveSize.Padding(3),
        backgroundColor:'#ffffff',
        width:'100%'
    },
    heading:{
      backgroundColor:'#ddd',
      paddingTop:7,
      paddingBottom:7,
      paddingLeft:10,
    },
    name:{
      fontSize:AppSizes.ResponsiveSize.Sizes(25),
      color:'#000',
      fontWeight:'600',
      marginBottom:10,
      fontFamily:'Roboto-Bold'
    },
    Report:{
      fontSize:AppSizes.ResponsiveSize.Sizes(23),
      color:'#000',
      fontWeight:'400',
      marginBottom:10,
      fontFamily:Fonts.Robotor
    },
    text:{
      fontSize:AppSizes.ResponsiveSize.Sizes(15),
      color:'#000',
      fontWeight:'400',
      marginBottom:5,
      fontFamily:Fonts.Robotor
    },
    box:{
      paddingLeft:30,
      paddingTop:20,
      paddingBottom:20,
    },
    Password:{
      fontSize:AppSizes.ResponsiveSize.Sizes(15),
      color:'#c41a39',
      fontWeight:'400',
      fontFamily:Fonts.Robotor
    },
    reportbox:{
      borderBottomWidth:1,
      paddingTop:20,
      paddingLeft:20,
      paddingBottom:10,
      borderBottomColor:'#000',
    },
    icon:{
      width:28,
      height:28
    }


});
