import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput,Picker,Alert} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import SquareButton from '../common/SquareButton';
import SelectButton from '../common/SelectButton';
import Header from '../common/Headerwithback'
import { Icon } from 'react-native-elements'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import CheckBox from 'react-native-check-box'
import SelectInput from '@tele2/react-native-select-input';
import ImagePicker from 'react-native-image-picker';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

import { Fonts } from '../../utils/Fonts';

import Swiper from 'react-native-swiper-animated';

const property = require('../../themes/Images/property.jpg')
const uploadimg = require('../../themes/Images/image-upload.png')


var radio_props = [
  {label: 'param1  ', value: 0 },
  {label: 'param2', value: 1 }
];
var radio_props1 = [
  {label: 'Within a week  ', value: 0 },
  {label: 'Within a month', value: 1 },
  {label: 'Not ready yet, just researching', value: 1 }
];

let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      avatarSource:null,
      uid:'',
      email:'',
      title:'',
      number:'',
      type:'',
      pet_breed:'',
      pet_color:'',
      age:'',
      gender:'',
      address:'',
      desc:''
    },
    _that = this;
  }

  componentWillMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        console.log('uid',uid);
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
        }
    })
  }

  PetBtn=()=>{
    _that.validationAndApiParameter()
    //const resetAction = NavigationActions.navigate({ routeName: 'HomeScreen'})
    //_that.props.navigation.dispatch(resetAction);
  }

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };
      ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        this.setState({
          avatarSource: source,
        });
      }
    });
   }

  validationAndApiParameter() {
        const { uid,title,email,number,type,pet_breed,pet_color,age,gender,avatarSource,address,desc } = this.state
        var error=0
        if(avatarSource==null){
          error=1
          Alert.alert('Input Error!','Please select a photo')
        }

        if(error==0){
          // var petData=JSON.stringify([{var_name:'name',value:title},{var_name:'image',value:avatarSource.uri},{var_name:'email',value:email},{var_name:'number',value:number},{var_name:'pet_type',value:type},{var_name:'pet_breed',value:pet_breed},{var_name:'color',value:pet_color},{var_name:'age',value:age},{var_name:'gender',value:gender},{var_name:'address',value:address},{var_name:'descp',value:desc}])
          // var data = {
          //   uid:uid,
          //   variable:petData
          // };
          const data = new FormData();
          data.append('method','saveAdoption')
          data.append('uid', uid);
          data.append('name', title);
          data.append('email', email);
          data.append('number', number);
          data.append('pet_type', type);
          data.append('pet_breed', pet_breed);
          data.append('color', pet_color);
          data.append('age', age);
          data.append('gender', gender);
          data.append('pet_image', {
            uri:  avatarSource.uri,
            type: 'image/jpeg', // or photo.type
            name: 'petImage.jpg'
          });
          data.append('address', address);
          data.append('descp', desc);
          console.log(data);
          this.postToApiCalling('POST', 'login', Constant.New_BASE, data);
        }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {

     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebServices(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((responseJson) => {
        console.log(responseJson);
          if ((!responseJson) || (responseJson.code == 0)) {
            console.log(responseJson);
            Alert.alert("Error",responseJson.message)
            _that.setState({ isVisible: false })
          }else {
            if (responseJson.code == 1) {
                _that.apiSuccessfullResponse(apiKey, responseJson)
            }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(()=>{
              Alert.alert("Server issue" + error);
          },200);
      });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    console.log(jsonRes);
    if (apiKey == 'login') {
      console.log(jsonRes);
      AsyncStorage.setItem('Animal_data',JSON.stringify(jsonRes.adopt_list))
      _that.props.navigation.navigate('HomeScreen')
    }
  }



  render() {
    const headerProp = {
      title: 'ADD ADOPTION',
      screens: '',
    };

    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>
        <ScrollView style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
          <View style={styles.centerbox}>
            <View style={styles.boxmain}>
              <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)} style={{height:120,width:120,marginLeft:'auto',marginRight:'auto',marginBottom:20}}>
                <View style={[styles.avatar,{ marginBottom: 20}]}>
                  {this.state.avatarSource === null ? (
                    <Image source={uploadimg} style={styles.productimg}/>
                  ) : (
                    <Image style={styles.avatar} source={this.state.avatarSource} />
                  )}
                </View>
              </TouchableOpacity>

                <TextInput
                    style={styles.input}
                    placeholder = "Title"
                    ref='title'
                    onChangeText={(title) => this.setState({title})}
                    value={this.state.title}
                    />
                <TextInput
                    style={styles.input}
                    ref='email'
                    placeholder = "Email Address"
                    keyboardType={'email-address'}
                    onChangeText={(email) => this.setState({email})}
                    value={this.state.email}
                />
                <TextInput
                    style={styles.input}
                    keyboardType={'phone-pad'}
                    ref='number'
                    placeholder = "Mobile Number"
                    onChangeText={(number) => this.setState({number})}
                    value={this.state.number}
                />
                <View style={styles.fullin}>
                  <Picker
                    selectedValue={this.state.type}
                    style={styles.input1}
                    onValueChange={(label, value) =>this.setState({type: label})}>
                    <Picker.Item label="Select pet type" value='0' />
                    <Picker.Item label="Dog" value='Dog' />
                    <Picker.Item label="Cat" value='Cat' />
                    <Picker.Item label="Aquatic Animal" value='Aquatic Animal' />
                    <Picker.Item label="Bird" value='Bird' />
                    <Picker.Item label="Small Mammal" value='5' />
                    <Picker.Item label="Large Mammal" value='6' />
                    <Picker.Item label="Amphibian" value='7' />
                    <Picker.Item label="Other" value='8' />

                  </Picker>
                </View>
                <TextInput
                  style={styles.input}
                  placeholder = "Pet Breed"
                  ref='pet_breed'
                  onChangeText={(pet_breed) => this.setState({pet_breed})}
                  value={this.state.pet_breed}
                />
                <TextInput
                  style={styles.input}
                  ref='pet_color'
                  placeholder = "Color"
                  onChangeText={(pet_color) => this.setState({pet_color})}
                  value={this.state.pet_color}
                />
                <TextInput
                  style={styles.input}
                  placeholder = "Age"
                  ref='age'
                  onChangeText={(age) => this.setState({age})}
                  value={this.state.age}
                />
                <View style={styles.fullin}>
                  <Picker
                    selectedValue={this.state.gender}
                    style={styles.input1}
                    onValueChange={(itemValue, itemIndex) =>
                      this.setState({gender: itemValue})
                    }>
                      <Picker.Item label="Gender" value="gender" />
                      <Picker.Item label="Male" value="Male" />
                      <Picker.Item label="Female" value="Female" />
                  </Picker>
                </View>
                <TextInput
                  style={styles.input}
                  placeholder = "Address"
                  ref='address'
                  onChangeText={(address) => this.setState({address})}
                  value={this.state.address}
                />
                <TextInput
                  style={styles.input}
                  placeholder = "Description"
                  ref='desc'
                  multiline={true}
                  numberOfLines={4}
                  onChangeText={(desc) => this.setState({desc})}
                  value={this.state.desc}
                />

                {/*<View style={styles.fullin}>
                    <Picker
                      selectedValue={this.state.language}
                      style={styles.input1}
                      onValueChange={(itemValue, itemIndex) =>this.setState({language: itemValue})}
                    >
                      <Picker.Item label="Status" value="java" />
                      <Picker.Item label="Neutered" value="js" />
                      <Picker.Item label="Not neutered" value="js" />
                  </Picker>
                </View>*/}
              </View>
            </View>
        </ScrollView>
        <TouchableOpacity style={styles.btnblksign} onPress={this.PetBtn} activeOpacity={.6} >
              <SquareButton label='Post'/>
          </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center',
    width:'100%',
    backgroundColor:'#fff'
  },
  content:{
    width:'100%',

  },
  centerbox:{
    width:'100%',
    alignItems:'center'
  },
  boxmain:{
    width:'90%',
    alignItems:'center',
    paddingTop:AppSizes.ResponsiveSize.Padding(2)
    //backgroundColor:'red'
  },
  input:{
    width:'100%',
    height:60,
    paddingLeft:0,
  borderBottomWidth:1,
  borderBottomColor:'#000',
  color:'#000',
  marginBottom:15,
    fontFamily:Fonts.Robotor
},
input1:{
  width:'100%',
  height:60,
  paddingLeft:0,
  color:'#a3a3a3',
  borderBottomWidth:1,
  borderBottomColor:'#a3a3a3',
  color:'#000',
},
label:{
  width:'100%',
  marginBottom:AppSizes.ResponsiveSize.Padding(4),
  fontSize:AppSizes.ResponsiveSize.Sizes(15),
  fontWeight:'400',
  color:'#000',
  fontFamily:Fonts.Robotor
},
quebox:{
  width:'100%',
  marginBottom:20
},
radioinput:{
  //marginBottom:10
//flexDirection:'row',
//justifyContent:'space-between'
},
btnblksign:{
  width:'100%'
},
inputdrop:{
  width:'100%',
  backgroundColor:'red'
},
fullin:{
  width:'100%',
  borderBottomWidth:1,
  borderBottomColor:'#000',
  marginBottom:15
  //backgroundColor:'red'
},
productimg:{
  height:120,
  width:120,
  marginBottom:15,
  marginLeft:'auto',
  marginRight:'auto',
},
martop:{
  marginTop:15
},
  avatar: {
   borderRadius: 75,
   width: 120,
   height: 120,
   marginBottom:15,
   marginLeft:'auto',
   marginRight:'auto',
  }
});
