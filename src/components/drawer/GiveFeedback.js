import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput,Alert} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import SquareButton from '../common/SquareButton';
import SelectButton from '../common/SelectButton';
import Header from '../common/Headerwithback'
import { Icon } from 'react-native-elements'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import CheckBox from 'react-native-check-box'
import SelectInput from '@tele2/react-native-select-input';

import { Fonts } from '../../utils/Fonts';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const property = require('../../themes/Images/property.jpg')
const uploadimg = require('../../themes/Images/image-upload.png')


let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      uid:'',
      text0:'',
      text1:'',
      text2:''
    },
    _that = this;
  }

  componentWillMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
        }
    })
  }

  validationAndApiParameter(apiKey) {
    if(apiKey=='saveData'){
      var error=0
      if(this.state.text0==''){
        error=1
        Alert.alert('Input Error!','Please enter your name')
      }
      else if(this.state.text1==''){
        error=1
        Alert.alert('Input Error!','Please enter your email address')
      }
      else if(this.state.text2==''){
        error=1
        Alert.alert('Input Error!','Please provide the suggestion')
      }
      if(error==0){
        var values=[{var_name:'name',value:this.state.text0},{var_name:'email',value:this.state.text1},{var_name:'suggestion',value:this.state.text2}]
        var data_array=JSON.stringify(values)
         var data = {
           uid:uid,
           variable:data_array,
         };
        // const data=new FormData()
        // data.append('uid',this.state.uid)
        // data.append('name',this.state.text0)
        // data.append('email',this.state.text1)
        // data.append('suggestion',this.state.text2)

        console.log(data);
        this.postToApiCalling('POST', 'saveData', Constant.URL_Feedback, data);
      }
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {

     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
        console.log(jsonRes);
          if ((!jsonRes) || (jsonRes.code == 0)) {
            console.log(jsonRes);
            Alert.alert("Alert",jsonRes.message)
            _that.setState({ isVisible: false })
          }else {
            if (jsonRes.code == 1) {
              console.log(jsonRes);
                _that.apiSuccessfullResponse(apiKey, jsonRes)
            }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(()=>{
              Alert.alert("Server issue" + error);
          },200);
      });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if(apiKey=='saveData'){
      this.setState({
        text0:'',
        text2:'',
        text1:'',
      })
      Alert.alert(
        'PAWH! Feedback',
        'Thank you for your feedback.',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
      );
    }
  }

  Homebtn(){
    _that.validationAndApiParameter('saveData')
  }

  render() {
    const headerProp = {
      title: 'Give Feedback',
      screens: '',
    };

    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>
        <ScrollView style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
          <View style={styles.centerbox}>
            <View style={styles.boxmain}>
              <View style={styles.inputbox}>
                  <Text style={styles.label}>Name</Text>
                  <TextInput
                      style={styles.input}
                      placeholder = "Name"
                      keyboardType={ 'default'}
                      placeholderTextColor={"#9E9E9E"}
                      onChangeText={(text0) => this.setState({text0})}
                      value={this.state.text0}
                      />
              </View>
              <View style={styles.inputbox}>
                    <Text style={styles.label}>Email Address</Text>
                    <TextInput
                        style={styles.input}
                        placeholder = "Email Address"
                        keyboardType={ 'email-address'}
                        placeholderTextColor={"#9E9E9E"}
                        onChangeText={(text1) => this.setState({text1})}
                        value={this.state.text1}
                        />
              </View>
              <View style={styles.inputbox}>
                  <Text style={styles.label}>Suggestions</Text>
                  <TextInput
                     style={styles.input1}
                     underlineColorAndroid="transparent"
                     placeholder={"Suggestions"}
                     keyboardType={ 'default'}
                     placeholderTextColor={"#9E9E9E"}
                     numberOfLines={3}
                     multiline={true}
                     onChangeText={(text2) => this.setState({text2})}
                     value={this.state.text2}
                   />
            </View>
              </View>
            </View>
        </ScrollView>
        <TouchableOpacity style={styles.btnblksign} onPress={()=>this.validationAndApiParameter('saveData')} activeOpacity={.6} >
              <SquareButton label='Submit'/>
          </TouchableOpacity>
      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center',
    width:'100%',
    backgroundColor:'#fff'
  },
  content:{
    width:'100%',

  },
  centerbox:{
    width:'100%',
    alignItems:'center'
  },
  boxmain:{
    width:'90%',
    alignItems:'center',
    marginBottom:50,
    paddingTop:AppSizes.ResponsiveSize.Padding(5)
    //backgroundColor:'red'

  },
  input:{
    width:'100%',
    height:40,
    paddingLeft:0,
  borderBottomWidth:1,
  borderBottomColor:'#a0a0a0',
  marginBottom:15,
  fontFamily:Fonts.Robotor
},
input1:{
  width:'100%',

  paddingLeft:0,
borderBottomWidth:1,
borderBottomColor:'#a0a0a0',
marginBottom:15,
fontFamily:Fonts.Robotor
},
label:{
  width:'100%',
  marginBottom:AppSizes.ResponsiveSize.Padding(2),
  fontSize:AppSizes.ResponsiveSize.Sizes(20),
  fontWeight:'400',
  color:'#c41a39',
  fontFamily:Fonts.Robotor
},
quebox:{
  width:'100%',
  marginBottom:20
},

btnblksign:{
  width:'100%'
},
productimg:{
  height:120,
  width:120,
  marginBottom:15,
  marginLeft:'auto',
  marginRight:'auto',
},
martop:{
  marginTop:15
},
  avatar: {
   borderRadius: 75,
   width: 120,
   height: 120,
   marginBottom:15,
   marginLeft:'auto',
   marginRight:'auto',
 },
 inputbox:{
   width:'100%'
 },
 label:{
   fontSize:AppSizes.ResponsiveSize.Sizes(15),
   color:'#000',
  fontFamily:'Roboto-Bold'
 }


});
