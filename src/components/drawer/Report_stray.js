import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput,Alert} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import SquareButton from '../common/SquareButton';
import SelectButton from '../common/SelectButton';
import Header from '../common/Headerwithback'
import { Icon } from 'react-native-elements'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import CheckBox from 'react-native-check-box'
import SelectInput from '@tele2/react-native-select-input';
import ImagePicker from 'react-native-image-picker';

import { Fonts } from '../../utils/Fonts';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

import Swiper from 'react-native-swiper-animated';

const property = require('../../themes/Images/property.jpg')
const uploadimg = require('../../themes/Images/image-upload.png')


let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      avatarSource:null,
      text:'',
      text1:'',
      text2:'',
      text3:'',
      text4:'',
      text5:'',
    },
    _that = this;
  }

  componentWillMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
        }
    })
  }

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };
      ImagePicker.showImagePicker(options, (response) => {

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        this.setState({
          avatarSource: source,
        });
      }
    });
   }

  validationAndApiParameter(apiKey) {
    if(apiKey=='saveData'){
      var error=0
      console.log(this.state.avatarSource.uri);
      if(this.state.avatarSource==null){
        error=1
        Alert.alert('Input Error!','Please select a photo')
      }
      // var myPet=JSON.stringify([{var_name:'dog_name',value:this.state.text},{var_name:'location',value:this.state.text1},{var_name:'description',value:this.state.text2},{var_name:'name',value:this.state.text3},{var_name:'mobile',value:this.state.text4},{var_name:'email',value:this.state.text5}])
      // var data={
      //   uid:uid,
      //   variable:myPet
      // }

      if(error==0){
        const data = new FormData();
        data.append('method','addreport')
        data.append('uid', this.state.uid);
        data.append('dog_name', this.state.text);
        data.append('location', this.state.text1);
        data.append('description', this.state.text2);
        data.append('name', this.state.text3);
        data.append('mobile', this.state.text4);
        data.append('email', this.state.text5);
        data.append('report_image', {
          uri:  this.state.avatarSource.uri,
          type: 'image/jpeg', // or photo.type
          name: 'petImage.jpg'
        });
        console.log(data);
        this.postToApiCalling('POST', 'saveData', Constant.New_BASE, data);
      }
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {
      //console.log(data);
      new Promise(function(resolve, reject) {
        if (method == 'POST') {
          resolve(WebServices.callWebServices(apiUrl, data));
        } else {
          resolve(WebServices.callWebService_GET(apiUrl, data));
        }
      }).then((responseJson) => {
        console.log(responseJson);
        _that.setState({ isVisible: false })
        if ((!responseJson) || (responseJson.code == 0)) {
            setTimeout(()=>{
              Alert.alert('Error',responseJson.message);
            },200);
        }
        else{
          if(responseJson.code == 1){
            _that.apiSuccessfullResponse(apiKey, responseJson)
          }
        }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(()=>{
              Alert.alert("Server issue"+error);
          },200);
      });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if(apiKey=='saveData'){
      this.setState({
        text:'',
        text1:'',
        text2:'',
        text3:'',
        text4:'',
        text5:'',
        avatarSource:null
      })
      Alert.alert(
        'Success',
        'Stray Has Been Reported',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
      );
      //_that.props.navigation.navigate('HomeScreen')
    }
  }

  Homebtn(){
    _that.validationAndApiParameter('saveData')
  }

  render() {
    const headerProp = {
      title: 'Report Stray',
      screens: '',
    };

    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>
        <ScrollView style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
          <View style={styles.centerbox}>
            <View style={styles.boxmain}>
              <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)} style={{height:120,width:120,marginLeft:'auto',marginRight:'auto',marginBottom:20}}>
                <View style={[styles.avatar,  styles.avatarContainer,{ marginBottom: 20}]}>
                  {this.state.avatarSource === null ? (
                    <Image source={uploadimg} style={styles.productimg}/>
                  ) : (
                    <Image style={styles.avatar} source={this.state.avatarSource} />
                  )}
                </View>
              </TouchableOpacity>
                <TextInput
                    style={styles.input}
                    placeholder = "Tag Name (If any)"
                    keyboardType={ 'default'}
                    onChangeText={(text) => this.setState({text})}
                    value={this.state.text}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder = "Location"
                        keyboardType={ 'default'}
                        onChangeText={(text1) => this.setState({text1})}
                        value={this.state.text1}
                    />

                    <TextInput
                        style={styles.input}
                        keyboardType={ 'default'}
                        placeholder = "Description"
                        onChangeText={(text2) => this.setState({text2})}
                        value={this.state.text2}
                    />

                  <Text style={[styles.label,styles.martop]}>
                          Enter your contact details
                  </Text>

                  <TextInput
                      style={styles.input}
                      placeholder = "Name"
                      keyboardType={ 'default'}
                      onChangeText={(text3) => this.setState({text3})}
                      value={this.state.text3}
                      />
                      <TextInput
                          style={styles.input}
                          keyboardType={ 'phone-pad'}
                          placeholder = "Mobile Number (Optional)"
                          onChangeText={(text4) => this.setState({text4})}
                          value={this.state.text4}
                      />

                      <TextInput
                          style={styles.input}
                          keyboardType={ 'email-address'}
                          placeholder = "Email Address"
                          onChangeText={(text5) => this.setState({text5})}
                          value={this.state.text5}
                      />
              </View>
            </View>
        </ScrollView>
        <TouchableOpacity style={styles.btnblksign} onPress={()=>this.Homebtn()} activeOpacity={.6} >
              <SquareButton label='Report'/>
          </TouchableOpacity>
      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center',
    width:'100%',
    backgroundColor:'#fff'
  },
  content:{
    width:'100%',

  },
  centerbox:{
    width:'100%',
    alignItems:'center'
  },
  boxmain:{
    width:'90%',
    alignItems:'center',
    marginBottom:50,
    paddingTop:AppSizes.ResponsiveSize.Padding(2)
    //backgroundColor:'red'

  },
  input:{
    width:'100%',
    height:40,
    paddingLeft:0,
  borderBottomWidth:1,
  borderBottomColor:'#a0a0a0',
  marginBottom:15,
  fontFamily:Fonts.Robotor
},
label:{
  width:'100%',
  marginBottom:AppSizes.ResponsiveSize.Padding(2),
  fontSize:AppSizes.ResponsiveSize.Sizes(20),
  fontWeight:'400',
  color:'#c41a39',
  fontFamily:Fonts.Robotor
},
quebox:{
  width:'100%',
  marginBottom:20
},

btnblksign:{
  width:'100%'
},
productimg:{
  height:120,
  width:120,
  marginBottom:15,
  marginLeft:'auto',
  marginRight:'auto',
},
martop:{
  marginTop:15
},
  avatar: {
   borderRadius: 75,
   width: 120,
   height: 120,
   marginBottom:15,
   marginLeft:'auto',
   marginRight:'auto',
  }


});
