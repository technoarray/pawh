import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput,Alert} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import SquareButton from '../common/SquareButton';
import SelectButton from '../common/SelectButton';
import Header from '../common/Headerwithback'
import { Icon } from 'react-native-elements'
import * as commonFunctions from '../../utils/CommonFunctions';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import CheckBox from 'react-native-check-box'

import { Fonts } from '../../utils/Fonts';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

import Swiper from 'react-native-swiper-animated';

const property = require('../../themes/Images/property.jpg')

var radio_props = [
  {label: 'Yes', value: 0 },
  {label: 'No', value: 1 }
];
var radio_props1 = [
  {label: 'Within a week  ', value: 0 },
  {label: 'Within a month', value: 1 },
  {label: 'Not ready yet, just researching', value: 2 }
];

let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      first_name:'',
      last_name:'',
      email:'',
      mobile_number:'',
      current_pet:0,
      ready_adopt:0,
      any_shelter:0,
      any_restrictions:0,
      uid:'',
      isChecked:false,
      isChecked1:false,
      isChecked2:false,
      isChecked3:false,
      isChecked4:false,
      aid:this.props.navigation.state.params.aid,
    },
    _that = this;
  }

  componentWillMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
        }
    })
  }

  validationAndApiParameter() {
        const { uid,aid,first_name,last_name,email,mobile_number,current_pet,ready_adopt,any_shelter,any_restrictions,isChecked,isChecked1,isChecked2,isChecked3,isChecked4} = this.state
        var error=0
        var reason_data=[]
        var c_pet=''
        var ready=''
        var shelter=''
        var restriction=''

        if(current_pet==1){
          c_pet='No'
        }else {
          c_pet='Yes'
        }

        if(ready_adopt==0){
          ready='Within a week'
        }
        else if (ready_adopt==1) {
          ready='Within a month'
        }
        else if(ready_adopt==2){
          ready='Not ready yet, just researching'
        }

        if(any_shelter==0){
          shelter='Yes'
        }
        else if (any_shelter==1) {
          shelter='No'
        }

        if(any_restrictions==0){
          restriction='Yes'
        }
        else if (any_restrictions==1) {
          restriction='No'
        }

        if(isChecked){
          reason_data.push('As a Family Pet')
        }
        if(isChecked1){
          reason_data.push('To train/use for hunting')
        }
        if(isChecked2){
          reason_data.push('As a gift for a friend, partner, or family member')
        }
        if(isChecked3){
          reason_data.push('As a companion for my other pet/pets')
        }
        if(isChecked4){
          reason_data.push('As a companion for my child/children')
        }

        if ((email.indexOf(' ') >= 0 || email.length <= 0)) {
          error=1;
          Alert.alert('Error','Please enter your email address.');
        }else if (!commonFunctions.validateEmail(email)) {
          error=1;
          Alert.alert('Error','Please enter valid Email address.');
        }
        if(error==0){
          var values=[{var_name:'aid',value:aid},{var_name:'first_name',value:first_name},{var_name:'last_name',value:last_name},{var_name:'email',value:email},{var_name:'mobile',value:mobile_number},{var_name:'current_pet',value:c_pet},{var_name:'ready_adopt',value:ready},{var_name:'any_shelter', value:shelter},{var_name:'any_restrictions',value:restriction},{var_name:'reason_adopt',value:reason_data.toString()}]
          var data_array=JSON.stringify(values)
           var data = {
             uid:uid,
             variable:data_array,
           };
           console.log(data);
           this.postToApiCalling('POST', 'request_adoption', Constant.URL_requestAdoption, data);
        }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {

     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
          if ((!jsonRes) || (jsonRes.code == 0)) {
            console.log(jsonRes);
            Alert.alert("Error",jsonRes.message)
          }else {
            if (jsonRes.code == 1) {
                _that.apiSuccessfullResponse(apiKey, jsonRes)
            }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          setTimeout(()=>{
              Alert.alert("Server issue" + error);
          },200);
      });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if (apiKey == 'request_adoption') {
      console.log(jsonRes);
      Alert.alert(
        'Thank you!',
        'Adoption submitted successfully.',
        [
          {
            text: 'Ok',
            onPress: () => _that.props.navigation.navigate('HomeScreen'),
            style: 'cancel',
          },
        ],
        {cancelable: false},
      );
    }
  }

  Homebtn=()=>{
    _that.validationAndApiParameter()
  }

  render() {
    const headerProp = {
      title: 'Request Adoption',
      screens: '',
    };

    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>
        <ScrollView style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
          <View style={styles.centerbox}>
            <View style={styles.boxmain}>
              <View style={styles.equalbox}>
                <View style={styles.equalin}>
                    <TextInput
                    style={styles.input}
                    placeholder = "First Name"
                    ref='first_name'
                    keyboardType={ 'default'}
                    onChangeText={(first_name) => this.setState({first_name})}
                    value={this.state.text}
                    />
                  </View>
                  <View style={styles.equalin}>
                    <TextInput
                        style={styles.input}
                        ref='last_name'
                        keyboardType={ 'default'}
                        placeholder = "Last Name"
                        onChangeText={(last_name) => this.setState({last_name})}
                        value={this.state.text1}
                    />
                  </View>
                </View>

                    <TextInput
                        style={styles.input}
                        ref='email'
                        autoCapitalize = "none"
                        keyboardType={ 'email-address'}
                        placeholder = "Email Address"
                        onChangeText={(email) => this.setState({email})}
                        value={this.state.email}
                    />
                    <TextInput
                      style={[styles.input,styles.botmar]}
                      placeholder = "Mobile Number"
                      ref='mobile_number'
                      autoCapitalize = "none"
                      keyboardType={ 'number-pad'}
                      onChangeText={(mobile_number) => this.setState({mobile_number})}
                    />
                    <View style={styles.quebox}>
                      <Text style={styles.label}>Do you currently own any pets?</Text>
                       <RadioForm
                         radio_props={radio_props}
                         initial={0}
                         buttonSize={10}
                         borderWidth={0.5}
                         style={styles.radioinput1}
                         onPress={(index,value) => {this.setState({current_pet:value}),console.log(value)}}
                       />
                   </View>
                   <View style={styles.quebox}>
                     <Text style={styles.label}>When will you be ready to adopt this animal?</Text>
                      <RadioForm
                        radio_props={radio_props1}
                        initial={0}
                        buttonSize={10}
                        borderWidth={0.5}
                        style={styles.radioinput}
                        onPress={(label) => {this.setState({ready_adopt:label})}}
                      />
                  </View>
                  <View style={styles.quebox}>
                    <Text style={styles.label}>Are you in contact with any other rescues or shelters about adoption?</Text>
                     <RadioForm
                       radio_props={radio_props}
                       initial={0}
                       buttonSize={10}
                       borderWidth={0.5}
                       style={styles.radioinput}
                       onPress={(label) => {this.setState({any_shelter:label})}}
                     />
                 </View>
                 <View style={styles.quebox}>
                   <Text style={styles.label}>Are there any restrictions on the owning of pets where you live?</Text>
                    <RadioForm
                      radio_props={radio_props}
                      initial={0}
                      buttonSize={10}
                      borderWidth={0.5}
                      style={styles.radioinput}
                      onPress={(label) => {this.setState({any_restrictions:label})}}
                    />
                </View>
                <View style={styles.quebox}>
                  <Text style={styles.label}>Please check all that apply. I want to adopt this animal as...</Text>
                  <CheckBox
                    style={{flex: 1,}}
                    onClick={()=>{this.setState({isChecked:!this.state.isChecked})}}
                    isChecked={this.state.isChecked}
                    rightText={"As a Family Pet"}
                    rightTextStyle={{color:'#000'}}
                  />
                  <CheckBox
                    style={{flex: 1, }}
                    onClick={()=>{this.setState({isChecked1:!this.state.isChecked1})}}
                    isChecked={this.state.isChecked1}
                    rightText={"To train/use for hunting"}
                    rightTextStyle={{color:'#000'}}
                  />
                  <CheckBox
                    style={{flex: 1, }}
                    onClick={()=>{
                    this.setState({isChecked2:!this.state.isChecked2})}}
                    isChecked={this.state.isChecked2}
                    rightText={"As a gift for a friend, partner, or family member"}
                    rightTextStyle={{color:'#000'}}
                  />
                  <CheckBox
                    style={{flex: 1}}
                    onClick={()=>{this.setState({isChecked3:!this.state.isChecked3})}}
                    isChecked={this.state.isChecked3}
                    rightText={"As a companion for my other pet/pets"}
                    rightTextStyle={{color:'#000'}}
                  />
                  <CheckBox
                    style={{flex: 1}}
                    onClick={()=>{this.setState({isChecked4:!this.state.isChecked4})}}
                    isChecked={this.state.isChecked4}
                    rightText={"As a companion for my child/children"}
                    rightTextStyle={{color:'#000'}}
                  />
               </View>


              </View>
            </View>
        </ScrollView>
        <TouchableOpacity style={styles.btnblksign} onPress={this.Homebtn} activeOpacity={.6} >
              <SquareButton label='Submit'/>
          </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center',
    width:'100%',
    backgroundColor:'#fff'
  },
  content:{
    width:'100%',

  },
  centerbox:{
    width:'100%',
    alignItems:'center'
  },
  boxmain:{
    width:'90%',
    alignItems:'center',
    paddingTop:AppSizes.ResponsiveSize.Padding(2)
    //backgroundColor:'red'
  },
  input:{
    width:'100%',
    height:60,
    paddingLeft:0,
  borderBottomWidth:1,
  borderBottomColor:'#a3a3a3',
  color:'#000',
  marginBottom:15,
  fontFamily:Fonts.Robotor
},
label:{
  width:'100%',
  marginBottom:AppSizes.ResponsiveSize.Padding(4),
  fontSize:AppSizes.ResponsiveSize.Sizes(15),
  fontWeight:'400',
  color:'#000',
fontFamily:Fonts.Robotor
},
quebox:{
  width:'100%',
  marginBottom:20,

},
radioinput:{
  //marginBottom:10
//flexDirection:'row',
//justifyContent:'space-between'
},
radioinput1:{
flexDirection:'row',
justifyContent:'space-between',
width:'50%'
},
btnblksign:{
  width:'100%'
},
equalbox:{
  flexDirection:'row',
  width:'100%',
  justifyContent:'space-between'
},
equalin:{
  width:'48%'
},
botmar:{
  marginBottom:30
}

});
