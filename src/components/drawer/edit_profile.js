import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput,Alert} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import SquareButton from '../common/SquareButton';
import CommonButton from '../common/CommonButton';
import Header from '../common/Headerwithback'
import { Icon } from 'react-native-elements'

import { Fonts } from '../../utils/Fonts';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

var home=require('../../themes/Images/2.png')
var pet=require('../../themes/Images/1.png')
var service=require('../../themes/Images/3.png')
var more=require('../../themes/Images/4.png')

let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      uid:'',
      confirm_pass:'',
      old_pass:'',
      new_pass:'',
      home:this.props.navigation.state.params.home,
      service:this.props.navigation.state.params.service,
      more:this.props.navigation.state.params.more,
      pet:this.props.navigation.state.params.pet
    },
    _that = this;
  }

  componentWillMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
        }
    })
  }

  Homebtn=()=>{
    this.setState({
      home:true,
      service:false,
      more:false,
      pet:false
    })
    _that.props.navigation.navigate('HomeScreen',{home:true,service:false,more:false,pet:false});
  }
  Servicebtn=()=>{
    this.setState({
      home:false,
      service:true,
      more:false,
      pet:false
    })
    _that.props.navigation.navigate('ServicesScreen',{home:false,service:true,more:false,pet:false});
  }

  Morebtn=()=>{
    this.setState({
      home:false,
      service:false,
      more:true,
      pet:false
    })
    _that.props.navigation.navigate('SettingScreen',{home:false,service:false,more:true,pet:false});
  }
  Petbtn=()=>{
    this.setState({
      home:false,
      service:false,
      more:false,
      pet:true
    })
    _that.props.navigation.navigate('Pet_detail_form',{home:false,service:false,more:false,pet:true});
  }

  validationAndApiParameter() {
        const { old_pass,new_pass,confirm_pass } = this.state
        var error=0

        if ((old_pass.indexOf(' ') >= 0 || old_pass.length <= 0)) {
          //this.setState({emailErrorMessage:'Please enter Email address!'});
          error=1;
          Alert.alert('Error','Please enter Current Password.');
        }else if ((new_pass.indexOf(' ') >= 0 || new_pass.length <= 0)) {
          error=1;
          Alert.alert('Error','Please enter New Password.');
        }
        else if ((confirm_pass.indexOf(' ') >= 0 || confirm_pass.length <= 0)) {
          error=1;
          Alert.alert('Error','Please enter Old Password.');
        }
        if(error==0){

           var data = {
             uid: uid,
             old_pass: old_pass,
             new_pass: new_pass,
             conf_pass:confirm_pass
           };
          console.log(data);
          this.postToApiCalling('POST', 'login', Constant.URL_changePass, data);
        }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {

     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
        console.log(jsonRes);
          if ((!jsonRes) || (jsonRes.code == 0)) {
            console.log(jsonRes);
            Alert.alert("Error",jsonRes.message)
            _that.setState({ isVisible: false })
          }else {
            if (jsonRes.code == 1) {
                _that.apiSuccessfullResponse(apiKey, jsonRes)
            }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(()=>{
              Alert.alert("Server issue" + error);
          },200);
      });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if (apiKey == 'login') {
      _that.props.navigation.navigate('SettingScreen',{home:false,service:false,more:true,pet:false});
    }
  }

  submit(){
    _that.validationAndApiParameter()
  }

  render() {
    const headerProp = {
      title: 'Edit Profile',
      screens: '',
    };

    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>
        <ScrollView style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
          <View style={styles.centerbox}>
            <View style={styles.boxmain}>
              <View style={styles.quebox}>
                <Text style={styles.label}>Current Password</Text>
                <TextInput
                  style={styles.input}
                  ref='old_pass'
                  placeholder = "Current Password"
                  autoCapitalize = "none"
                  secureTextEntry = {true}
                  onChangeText={(old_pass) => this.setState({old_pass})}
                  value={this.state.old_pass}
                />
              </View>
              <View style={styles.quebox}>
                 <Text style={styles.label}>New Password</Text>
                 <TextInput
                   style={styles.input}
                   ref='new_pass'
                   placeholder = "New Password"
                   autoCapitalize = "none"
                   secureTextEntry = {true}
                   onChangeText={(new_pass) => this.setState({new_pass})}
                   value={this.state.new_pass}
                 />
              </View>
              <View style={styles.quebox}>
                <Text style={styles.label}>Confirm Password</Text>
                <TextInput
                  style={styles.input}
                  ref='confirm_pass'
                  placeholder = "Confirm Password"
                  autoCapitalize = "none"
                  secureTextEntry = {true}
                  onChangeText={(confirm_pass) => this.setState({confirm_pass})}
                  value={this.state.confirm_pass}
                />
              </View>
              <TouchableOpacity style={styles.full_btn} onPress={()=> this.submit()}>
                <CommonButton label='Submit'/>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>

        <View style={{backgroundColor:'#c41a39',zIndex:9999,width:'100%',height:50,flexDirection:'row',justifyContent:'space-around',position:'absolute',bottom:0}}>
          <TouchableOpacity onPress={this.Homebtn}  style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.home?{opacity:1}:{opacity:0.5}]}>
            <Image source={home} style={styles.icon}/>
            <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9)}}>Adopt</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.Petbtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.pet?{opacity:1}:{opacity:0.5}]}>
            <Image source={pet} style={styles.icon}/>
            <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9)}}>My Pet</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.Servicebtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.service?{opacity:1}:{opacity:0.5}]}>
            <Image source={service} style={styles.icon}/>
            <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9)}}>Services</Text>
          </TouchableOpacity>
          <TouchableOpacity  onPress={this.Morebtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.more?{opacity:1}:{opacity:0.5}]}>
            <Image source={more} style={styles.icon}/>
            <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9)}}>More</Text>
          </TouchableOpacity>
        </View>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center',
    width:'100%',
    backgroundColor:'#fff'
  },
  content:{
    width:'100%',

  },
  centerbox:{
    width:'100%',
    alignItems:'center'
  },
  boxmain:{
    width:'90%',
    alignItems:'center',
    paddingTop:AppSizes.ResponsiveSize.Padding(4),
    //backgroundColor:'red'
  },
  input:{
    width:'100%',
    height:40,
    paddingLeft:0,
    borderBottomWidth:1,
    borderBottomColor:'#a0a0a0',
    marginBottom:15,
      fontFamily:Fonts.Robotor
  },
  label:{
    width:'100%',
    marginBottom:AppSizes.ResponsiveSize.Padding(2),
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
    fontWeight:'600',
    color:'#000',
      fontFamily:'Roboto-Medium'
  },
  quebox:{
    width:'100%',
    marginBottom:10
  },

  btnblksign:{
    width:'100%'
  },
  full_btn:{
    width:'100%'
  },
  icon:{
    width:28,
    height:28
  }
});
