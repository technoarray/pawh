import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput,BackHandler,Alert} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import SquareButton from '../common/SquareButton';
import SelectButton from '../common/SelectButton';
import Header from '../common/Headerwithback'
import { Icon } from 'react-native-elements'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import BottomTabs from '../common/BottomTabs'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

import { Fonts } from '../../utils/Fonts';

var home=require('../../themes/Images/2.png')
var pet=require('../../themes/Images/1.png')
var service=require('../../themes/Images/3.png')
var more=require('../../themes/Images/4.png')

const property = require('../../themes/Images/property.jpg')
const uploadimg = require('../../themes/Images/image-upload.png')
const edit = require('../../themes/Images/pencil-edit-button.png')
var data=[]
let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      home:this.props.navigation.state.params.home,
      service:this.props.navigation.state.params.service,
      more:this.props.navigation.state.params.more,
      pet:this.props.navigation.state.params.pet,
      data:[]
    },
    _that = this;
  }

  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      _that.props.navigation.navigate('HomeScreen')
      return true;
    });

    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
          _that.validationAndApiParameter('Show',uid)
        }
    })
  }

  componentDidMount() {
    this.subs = this.props.navigation.addListener("didFocus", () =>
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
          _that.validationAndApiParameter('Show',uid)
        }
    })
    );
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  validationAndApiParameter(apiKey,uid) {
    if(apiKey=='Show'){
      var data={
        uid:uid
      }
      console.log(data);
      this.postToApiCalling('POST', 'Save', Constant.URL_showMyPet, data);
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {
    console.log('hi');
     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
        console.log(jsonRes);
          if ((!jsonRes) || (jsonRes.code == 0)) {
            console.log(jsonRes);
            Alert.alert("Error",jsonRes.message)
            _that.setState({ isVisible: false })
          }else {
            if (jsonRes.code == 1) {
              _that.apiSuccessfullResponse(apiKey, jsonRes)
            }
            else if (jsonRes.code==2) {
              _that.apiSuccessfullResponse(apiKey, jsonRes)
            }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(()=>{
              Alert.alert("Server issue" + error);
          },200);
      });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    data=jsonRes.mypet[0]
    this.setState({data:jsonRes.mypet[0]})
  }


  Homebtn=()=>{
    this.setState({
      home:true,
      service:false,
      more:false,
      pet:false
    })
    _that.props.navigation.navigate('HomeScreen',{home:true,service:false,more:false,pet:false});
  }
  Servicebtn=()=>{
    this.setState({
      home:false,
      service:true,
      more:false,
      pet:false
    })
    _that.props.navigation.navigate('ServicesScreen',{home:false,service:true,more:false,pet:false});
  }

  Morebtn=()=>{
    this.setState({
      home:false,
      service:false,
      more:true,
      pet:false
    })
    _that.props.navigation.navigate('SettingScreen',{home:false,service:false,more:true,pet:false});
  }
  Petbtn=()=>{
    this.setState({
      home:false,
      service:false,
      more:false,
      pet:true
    })
    _that.props.navigation.navigate('Pet_detail_form',{home:false,service:false,more:false,pet:true});
  }

  AddBtn=()=>{
    _that.props.navigation.navigate('Edit_pet_detail',{data:this.state.data});
  }


  render() {
    const headerProp = {
      title: 'WELCOME',
      screens: '',
    };

    return (

      <View style={styles.container}>

        <ScrollView style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
          <View style={styles.centerbox}>
            <View style={styles.boxmain}>
                <Image source={{uri: Constant.image_path2+data.image}} style={styles.productimg}/>
                <View  style={[styles.box,styles.shadow]}>
                <View style={{marginTop:-60, alignSelf:'flex-end',zIndex:11111,}}>
                  <TouchableOpacity  onPress={this.AddBtn}  style={styles.editicon}>
                      <Image source={edit} style={styles.edit_icon}/>
                  </TouchableOpacity>
                </View>
                  <View style={styles.petname}>
                        <Text style={styles.name}>{data.name}</Text>
                  </View>
                  <View style={styles.detail}>
                      <View style={styles.block}>
                            <Text style={styles.heading}>Breed</Text>
                            {data.breed!=''?<Text style={styles.dec}>{data.breed}</Text>:<Text style={styles.dec}>N/A</Text>}
                      </View>
                      <View style={styles.block}>
                            <Text style={styles.heading}>Age</Text>
                            {data.age!=''?<Text style={styles.dec}>{data.age}</Text>:<Text style={styles.dec}>N/A</Text>}
                      </View>
                  </View>
                  <View style={styles.detail}>
                      <View style={styles.block}>
                            <Text style={styles.heading}>Gender</Text>
                            {data.gender!=''?<Text style={styles.dec}>{data.gender}</Text>:<Text style={styles.dec}>N/A</Text>}
                      </View>
                      <View style={styles.block}>
                            <Text style={styles.heading}>Status</Text>
                            {data.status!=''?<Text style={styles.dec}>{data.status}</Text>:<Text style={styles.dec}>N/A</Text>}
                      </View>
                  </View>
                  <View style={styles.detail}>
                      <View style={styles.block}>
                            <Text style={styles.heading}>Weight</Text>
                            {data.weight!=''?<Text style={styles.dec}>{data.weight} lb</Text>:<Text style={styles.dec}>N/A</Text>}
                      </View>
                      <View style={styles.block}>
                            <Text style={styles.heading}>Height</Text>
                            {data.height!=''?<Text style={styles.dec}>{data.height} inches</Text>:<Text style={styles.dec}>N/A</Text>}
                      </View>
                  </View>
                  <View style={styles.detail}>
                      <View style={styles.block}>
                            <Text style={styles.heading}>Color</Text>
                            {data.color!=''?<Text style={styles.dec}>{data.color}</Text>:<Text style={styles.dec}>N/A</Text>}
                      </View>
                      <View style={styles.block}>

                      </View>

                  </View>
                </View>
              </View>
            </View>
        </ScrollView>

        <View style={{backgroundColor:'#c41a39',zIndex:9999,width:'100%',height:50,flexDirection:'row',justifyContent:'space-around',position:'absolute',bottom:0}}>
          <TouchableOpacity onPress={this.Homebtn}  style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.home?{opacity:1}:{opacity:0.5}]}>
            <Image source={home} style={styles.icon}/>
            <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>Adopt</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.Petbtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.pet?{opacity:1}:{opacity:0.5}]}>
            <Image source={pet} style={styles.icon}/>
            <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>My Pet</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.Servicebtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.service?{opacity:1}:{opacity:0.5}]}>
            <Image source={service} style={styles.icon}/>
            <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>Services</Text>
          </TouchableOpacity>
          <TouchableOpacity  onPress={this.Morebtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.more?{opacity:1}:{opacity:0.5}]}>
            <Image source={more} style={styles.icon}/>
            <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>More</Text>
          </TouchableOpacity>
        </View>

      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center',
    width:'100%',
    backgroundColor:'#fff'
  },
  content:{
    width:'100%',

  },
  centerbox:{
    width:'100%',
    alignItems:'center'
  },
  boxmain:{
    width:'100%',
    alignItems:'center',

    //backgroundColor:'red'
  },

productimg:{
  height:AppSizes.screen.height/3,
  width:'100%',
  marginBottom:15
},
btnblksign:{
  width:'100%'
},
box:{
  width:'100%',

  alignItems:'center',
  marginBottom:10,
  //flexDirection:'row',
  //height:AppSizes.ResponsiveSize.height/2
  paddingLeft:AppSizes.ResponsiveSize.Padding(8),
paddingRight:AppSizes.ResponsiveSize.Padding(8),
  paddingTop:AppSizes.ResponsiveSize.Padding(6),
  paddingBottom:AppSizes.ResponsiveSize.Padding(2),
  textAlign:'center',

  position:'relative'
},

name:{
  fontSize:AppSizes.ResponsiveSize.Sizes(25),
  marginBottom:AppSizes.ResponsiveSize.Padding(1),
  color:'#c41a39',
  fontWeight:'700',
  fontFamily:'Roboto-Bold'
},
heading:{
  fontSize:AppSizes.ResponsiveSize.Sizes(18),
  color:'#a3a3a3',
marginBottom:AppSizes.ResponsiveSize.Padding(1),
fontWeight:'400',
fontFamily:Fonts.Robotor
  },
  dec:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    color:'#000',
  marginBottom:AppSizes.ResponsiveSize.Padding(1),
fontWeight:'400',
fontFamily:Fonts.Robotor
    },
    detail:{
      flexDirection:'row',
    },
  block:{
    width:'50%',
    paddingTop:AppSizes.ResponsiveSize.Padding(3),
    paddingBottom:AppSizes.ResponsiveSize.Padding(3),
    //alignItems:'center',
    justifyContent:'flex-start'
    //backgroundColor:'red'
  },
  editicon:{
    backgroundColor:'#c41a39',
    borderRadius:50,
    padding:15,


  },
  edit_icon:{
    width:35,
    height:35
  },
  icon:{
    width:28,
    height:28
  }

});
