import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import SelectButton from '../common/SelectButton';

import { Fonts } from '../../utils/Fonts';

var Constant = require('../../api/ApiRules').Constant;

const background = require('../../themes/Images/background.png')
const logo = require('../../themes/Images/logo.png')



let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
    },
    _that = this;
  }
  componentWillMount() {
      AsyncStorage.getItem('loggedIn').then((value) => {
            var loggedIn = JSON.parse(value);
            if(loggedIn){
              _that.props.navigation.navigate('HomeScreen',{home:true,service:false,more:false,pet:false});
            }
        })
    }

  Loginbtn=()=>{
    const resetAction = NavigationActions.navigate({ routeName: 'SigninScreen'})
    _that.props.navigation.dispatch(resetAction);
  }
  Signupbtn=()=>{
    const resetAction = NavigationActions.navigate({ routeName: 'SignUpScreen'})
    _that.props.navigation.dispatch(resetAction);
  }

  render() {
    return (

      <View style={styles.container}>

          <ImageBackground source={background} style={{width: '100%', height: '100%'}}>
              <View style={styles.logosec}>

                    <Image source={logo} style={styles.logo}/>

                <TouchableOpacity style={styles.btnblk} onPress={this.Loginbtn} activeOpacity={.6} >
                    <View  style={{width:AppSizes.screen.width/1.1, marginBottom:20,  height:AppSizes.screen.width/7,alignItems:'center', justifyContent: 'center',backgroundColor:'#c41a39',paddingLeft:AppSizes.ResponsiveSize.Padding(1),paddingRight:AppSizes.ResponsiveSize.Padding(1),borderRadius:50,shadowOffset: { width: 2, height: 2 },
                    shadowOpacity: 0.5,
                    shadowRadius: 3,
                    elevation: 2,
                    zIndex:0,}}>
                        <Text style={styles.textbtnn}>Sign In</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btnblk} onPress={this.Signupbtn} activeOpacity={.6} >
                    <View  style={{width:AppSizes.screen.width/1.1, height:AppSizes.screen.width/7,alignItems:'center',borderColor:'#fff', borderWidth:1.50,justifyContent: 'center',backgroundColor:'transparent',paddingLeft:AppSizes.ResponsiveSize.Padding(1),paddingRight:AppSizes.ResponsiveSize.Padding(1),borderRadius:50,shadowOffset: { width: 2, height: 2 },
                    shadowOpacity: 0.5,
                    shadowRadius: 3,
                    elevation: 2,
                    zIndex:0,}}>
                        <Text style={styles.textbtn}>Create account </Text>
                    </View>
                </TouchableOpacity>

              </View>

          </ImageBackground>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center'
  },
  logosec:{
      flex: 1,
      justifyContent:'center',
      //backgroundColor:'red',
      alignItems:'center',
      width:'100%'
  },
  logo:{
    width:180,
    height:175,
    marginBottom:AppSizes.ResponsiveSize.Padding(5)
  },

  bottomblk:{
    alignItems:'center',
    backgroundColor:'green',
    flex:1,
    marginBottom:30,
  },
  textbtn:{
    color:'#fff',
    fontSize:AppSizes.ResponsiveSize.Sizes(18),

  },
  textbtnn:{
    color:'#fff',
    fontSize:AppSizes.ResponsiveSize.Sizes(18),

  }
});
