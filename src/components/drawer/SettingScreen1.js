import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import CommonButton from '../common/CommonButton';
import SelectButton from '../common/SelectButton';
import Header from '../common/Headerwithback'
import FlipToggle from 'react-native-flip-toggle-button';
//import Icon from 'react-native-vector-icons/FontAwesome';
import BottomTabs from '../common/BottomTabs'

import { Fonts } from '../../utils/Fonts';

var Constant = require('../../api/ApiRules').Constant;

import Swiper from 'react-native-swiper-animated';
const home = require('../../themes/Images/home.png')
const left = require('../../themes/Images/right-black.png')

let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
    },
    _that = this;
  }

  profileBtn=()=>{
    const resetAction = NavigationActions.navigate({ routeName: 'edit_profile'})
    _that.props.navigation.dispatch(resetAction);
  }

  ReportBtn=()=>{
    const resetAction = NavigationActions.navigate({ routeName: 'Report_stray'})
    _that.props.navigation.dispatch(resetAction);
  }

  render() {
    const headerProp = {
      title: 'More',
      screens: '',
    };

    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>

        <View style={styles.content}>
            <View style={styles.container1}>
              <View style={styles.debox}>
                <Text style={styles.name}>
                    John
                </Text>
                <Text style={styles.username}>
                    John Doe
                </Text>
                <Text style={styles.username}>
                    John@gmail.com
                </Text>

              <TouchableOpacity activeOpacity={.6} onPress={this.profileBtn} style={styles.questionbox}>
                    <View style={{width:'75%'}}>
                        <Text style={styles.questionheading}>
                            Change Password
                        </Text>
                    </View>
                    <View style={{width:'25%',alignItems:'flex-end',paddingRight:AppSizes.ResponsiveSize.Padding(8)}}>
                        <View style={styles.sideicon}>
                            <Image source={left} style={[styles.ImageStyle,styles.sidearrow]} />
                        </View>
                    </View>
                </TouchableOpacity>

                <View style={styles.questionbox}>
                      <TouchableOpacity onPress={this.ReportBtn} style={{width:'100%'}}>
                          <Text style={styles.questionheading}>
                              Report Strays
                          </Text>
                      </TouchableOpacity>

                  </View>
</View>
            </View>
        </View>
<BottomTabs  style={{position:'absolute',bottom:0}}/>
      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center',
    width:'100%',
    backgroundColor:'#ffffff'
  },

    content: {
        flex: 1,
        flexDirection: 'column',
        height:'70%',
        //paddingTop:AppSizes.ResponsiveSize.Padding(3),
        backgroundColor:'#ffffff'
    },
    container1:{
      height:AppSizes.screen.height/4,
      //backgroundColor:'red',
      alignItems:'center',
      paddingTop:15
    },
    questionbox:{
    //  backgroundColor:'green',
    marginTop:AppSizes.ResponsiveSize.Padding(3),
      height:50,
      flexDirection:'row',

      justifyContent:'center',



    },
    questionheading:{
      fontSize:AppSizes.ResponsiveSize.Sizes(15),
      fontWeight:'400',
      //textTransform:'uppercase'
      color:'#000',
      fontFamily:Fonts.Robotor
    },
    name:{
      fontSize:AppSizes.ResponsiveSize.Sizes(25),
      fontWeight:'600',
      //textTransform:'uppercase'
      color:'#000',
      fontFamily:'Roboto-Bold'
    },
    username:{
      fontSize:AppSizes.ResponsiveSize.Sizes(15),
      fontWeight:'400',
      //textTransform:'uppercase'
      color:'#000',
      fontFamily:Fonts.Robotor
    },

    sideicon:{
        width:'10%',
        alignItems: 'center',
        paddingTop:AppSizes.ResponsiveSize.Padding(2),
        //backgroundColor:'red'
    },
    ImageStyle: {
      height: AppSizes.screen.width/18,
      width: AppSizes.screen.width/18,
      resizeMode : 'contain',
    },
    logobox:{
      width:'100%',
      backgroundColor:'#ddd',
      alignItems:'center',
      justifyContent:'center',
      height:AppSizes.screen.height/4
    },
    logo:{
      width:AppSizes.screen.width/2,
      height:AppSizes.screen.height/10

    },
    debox:{
      width:'90%'
    }

});
