import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput,Alert} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import CommonButton from '../common/CommonButton';
import SelectButton from '../common/SelectButton';
import Header from '../common/Header'
import { Icon } from 'react-native-elements'
import BottomTabs from '../common/BottomTabs'
import Swiper from 'react-native-swiper-animated';

import { Fonts } from '../../utils/Fonts';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const property = require('../../themes/Images/property.jpg')

var home=require('../../themes/Images/2.png')
var pet=require('../../themes/Images/1.png')
var service=require('../../themes/Images/3.png')
var more=require('../../themes/Images/4.png')

let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      home:this.props.navigation.state.params.home,
      service:this.props.navigation.state.params.service,
      more:this.props.navigation.state.params.more,
      pet:this.props.navigation.state.params.pet,
      adopt_list:[],
      nrf:false
    },
    _that = this;
  }

  componentWillMount(){
    console.log('hi');
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
          _that.validationAndApiParameter(uid,'getData')
        }
    })
  }

  componentDidMount() {
    this.subs = this.props.navigation.addListener("didFocus", () =>
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
          _that.validationAndApiParameter(uid,'getData')
        }
    })
    );
  }

  Homebtn=()=>{
    this.setState({
      home:true,
      service:false,
      more:false,
      pet:false
    })
    _that.props.navigation.navigate('HomeScreen',{home:true,service:false,more:false,pet:false});
  }
  Servicebtn=()=>{
    this.setState({
      home:false,
      service:true,
      more:false,
      pet:false,
    })
    _that.props.navigation.navigate('ServicesScreen',{home:false,service:true,more:false,pet:false});
  }

  validationAndApiParameter(uid,apiKey) {
    if(apiKey=='getData'){
      var data={
        uid:uid
      }
      console.log(data);
      this.postToApiCalling('POST', 'getData', Constant.URL_showPetData, data);
    }
    if(apiKey=='fav'){
      var data={
        uid:this.state.uid,
        aid:uid
      }
      console.log(data);
      this.postToApiCalling('POST', 'fav', Constant.URL_fav, data);
    }
    if(apiKey == 'unlike'){
      var data={
        uid:this.state.uid,
        aid:uid
      }
      console.log(data);
      this.postToApiCalling('POST', 'unlike', Constant.URL_Unlike, data);
    }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {

     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
        console.log(jsonRes);
          if ((!jsonRes) || (jsonRes.code == 0)) {
            console.log(jsonRes);
            Alert.alert("Alert",jsonRes.message)
          }else {
            if (jsonRes.code == 1) {
              _that.setState({nrf:false})
                _that.apiSuccessfullResponse(apiKey, jsonRes)
            }
            else if(jsonRes.code == 2){
              _that.setState({nrf:true})
            }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          setTimeout(()=>{
              Alert.alert("Server issue" + error);
          },200);
      });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if(apiKey=='getData'){
      console.log(jsonRes);
      this.setState({adopt_list:jsonRes.adopt_list})
    }
    if(apiKey=='fav'){
      console.log(jsonRes);
      this.setState({adopt_list:jsonRes.adopt_list})
      //Alert.alert("Alert",jsonRes.message)
    }
    if(apiKey=='unlike'){
      console.log(jsonRes);
      this.setState({adopt_list:jsonRes.adopt_list})
      //Alert.alert("Alert",jsonRes.message)
    }
  }

  Morebtn=()=>{
    this.setState({
      home:false,
      service:false,
      more:true,
      pet:false
    })
    _that.props.navigation.navigate('SettingScreen',{home:false,service:false,more:true,pet:false});
  }
  Petbtn=()=>{
    this.setState({
      home:false,
      service:false,
      more:false,
      pet:true
    })
    _that.props.navigation.navigate('Pet_detail_form',{home:false,service:false,more:false,pet:true});
  }

  Addbtn=()=>{
    const resetAction = NavigationActions.navigate({ routeName: 'Add_adoption'})
    _that.props.navigation.dispatch(resetAction);
  }


  render() {
    const headerProp = {
      title: 'Adopt',
      screens: '',
    };

    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>


            <ScrollView style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
                <View style={styles.mainbox}>
                {this.state.nrf?
                  <Text style={styles.price}>No Record Found</Text>
                :
                  this.state.adopt_list.map((data)=>
                    <TouchableOpacity  style={[styles.box,styles.shadow]} key={data.adoptid} onPress={()=>(this.setState({adopt_list:[]}),_that.props.navigation.navigate('SingleScreen',{home:true,service:false,more:false,pet:false,pet_id:data.adoptid}))}>
                        <View style={styles.imgbox}>
                            <Image source={{uri: Constant.image_path+data.image}} style={styles.productimg}/>
                        </View>
                        <View style={styles.contentbox}>
                          <Text style={styles.price}>{data.name}</Text>
                          {data.gender==''?<Text style={styles.gender}><Text style={styles.bold}>Gender</Text> - N/A</Text>:<Text style={styles.gender}><Text style={styles.bold}>Gender</Text> - {data.gender}</Text>}
                          {data.pet_breed==''?<Text style={styles.breed}><Text style={styles.bold}>Breed</Text>- N/A</Text>:<Text style={styles.breed}><Text style={styles.bold}>Breed</Text>- {data.pet_breed}</Text>}
                          {data.address==''?<Text style={styles.location}><Text style={styles.bold}>Location</Text>- N/A</Text>:<Text style={styles.location}><Text style={styles.bold}>Location</Text>- {data.address}</Text>}
                          {data.islike==null?
                            <TouchableOpacity onPress={() => _that.validationAndApiParameter(data.adoptid,'fav')} style={styles.favicon}>
                                <Icon
                                  raised
                                  sizes={18}
                                  name='heart'
                                  type='font-awesome'
                                  color='#999'
                                />
                            </TouchableOpacity>
                          :
                            <TouchableOpacity onPress={() => _that.validationAndApiParameter(data.adoptid,'unlike')} style={styles.favicon}>
                                <Icon
                                  raised
                                  sizes={18}
                                  name='heart'
                                  type='font-awesome'
                                  color='#c41a39'
                                />
                            </TouchableOpacity>
                          }

                        </View>
                    </TouchableOpacity>
                  )
                }


                </View>
            </ScrollView>

            <View style={{backgroundColor:'#c41a39',zIndex:9999,width:'100%',height:50,flexDirection:'row',justifyContent:'space-around',position:'absolute',bottom:0}}>
              <TouchableOpacity onPress={this.Homebtn}  style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.home?{opacity:1}:{opacity:0.5}]}>
                <Image source={home} style={styles.icon}/>
                <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9)}}>Adopt</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.Petbtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.pet?{opacity:1}:{opacity:0.5}]}>
                <Image source={pet} style={styles.icon}/>
                <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9)}}>My Pet</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.Servicebtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.service?{opacity:1}:{opacity:0.5}]}>
                <Image source={service} style={styles.icon}/>
                <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9)}}>Services</Text>
              </TouchableOpacity>
              <TouchableOpacity  onPress={this.Morebtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.more?{opacity:1}:{opacity:0.5}]}>
                <Image source={more} style={styles.icon}/>
                <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9)}}>More</Text>
              </TouchableOpacity>
            </View>
      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center',
    width:'100%',
    backgroundColor:'#fff'
  },
  content: {
      flex: 1,
      width:'100%',
      height:'100%',
      position:'relative'
  },
  box:{
    width:'90%',
    backgroundColor:'#f2f2f2',
    alignItems:'center',
    marginBottom:15,
    flexDirection:'row',
    //height:AppSizes.ResponsiveSize.height/2
    padding:10,
    borderRadius:15,
    textAlign:'center',
    position:'relative'
  },
  icon1:{
    width:AppSizes.screen.width/20,
      height:AppSizes.screen.height/40,
  },

  price:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    marginBottom:AppSizes.ResponsiveSize.Padding(1),
    color:'#000000',

    fontFamily:'Roboto-Bold',

  },
  gender:{
    fontSize:AppSizes.ResponsiveSize.Sizes(10),
    color:'#333',
marginBottom:AppSizes.ResponsiveSize.Padding(.50),
   fontFamily:Fonts.Robotor
    },
    bold:{
fontWeight:'700',
fontFamily:'Roboto-Bold',
    },
    breed:{
      fontSize:AppSizes.ResponsiveSize.Sizes(10),
      color:'#333',
marginBottom:AppSizes.ResponsiveSize.Padding(.50),
   fontFamily:Fonts.Robotor
      },
      location:{
        fontSize:AppSizes.ResponsiveSize.Sizes(10),
        color:'#333',
  marginBottom:AppSizes.ResponsiveSize.Padding(.50),
     fontFamily:Fonts.Robotor
        },
  imgbox:{
    width:'30%',
    height:100,
  },
  contentbox:{
    width:'70%',
    paddingLeft:10
  },
  productimg:{
    width:'100%',
    height:'100%',
    borderRadius:5
  },
  mainbox:{
    width:'100%',
    alignItems:'center',
    paddingTop:AppSizes.ResponsiveSize.Padding(3),
    marginBottom:80
  },
  shadow:{
    borderWidth:0,
    borderRadius: 5,
    justifyContent:'center',
    backgroundColor:'#f2f2f2',
    borderColor: '#999',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.5,
    shadowRadius: 3,
    elevation: 2,
    zIndex:0,
 },
 favicon:{
   position:'absolute',
   right:10,
   top:0
 },
 plus:{
   position:'absolute',
   right:15,
   height:45,
   width:45,
   bottom:15,
 },
 icon:{
   width:28,
   height:28
 }

});
