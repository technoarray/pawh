import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import CommonButton from '../common/CommonButton';
import SelectButton from '../common/SelectButton';
import Header from '../common/Header'
import { Icon } from 'react-native-elements'
import BottomTabs from '../common/BottomTabs'
var Constant = require('../../api/ApiRules').Constant;

import { Fonts } from '../../utils/Fonts';

const ee = require('../../themes/Images/ee.png')
const aa = require('../../themes/Images/aa.png')
const bb = require('../../themes/Images/bb.png')
const cc = require('../../themes/Images/cc.png')
const dd = require('../../themes/Images/dd.png')
const logo = require('../../themes/Images/logo.png')

var home=require('../../themes/Images/2.png')
var pet=require('../../themes/Images/1.png')
var service=require('../../themes/Images/3.png')
var more=require('../../themes/Images/4.png')

let _that
export default class ServicesScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      home:this.props.navigation.state.params.home,
      service:this.props.navigation.state.params.service,
      more:this.props.navigation.state.params.more,
      pet:this.props.navigation.state.params.pet
    },
    _that = this;
  }

  Homebtn=()=>{
    this.setState({
      home:true,
      service:false,
      more:false,
      pet:false
    })
    _that.props.navigation.navigate('HomeScreen',{home:true,service:false,more:false,pet:false});
  }
  Servicebtn=()=>{
    this.setState({
      home:false,
      service:true,
      more:false,
      pet:false
    })
    _that.props.navigation.navigate('ServicesScreen',{home:false,service:true,more:false,pet:false});
  }

  Morebtn=()=>{
    this.setState({
      home:false,
      service:false,
      more:true,
      pet:false
    })
    _that.props.navigation.navigate('SettingScreen',{home:false,service:false,more:true,pet:false});
  }
  Petbtn=()=>{
    this.setState({
      home:false,
      service:false,
      more:false,
      pet:true
    })
    _that.props.navigation.navigate('Pet_detail_form',{home:false,service:false,more:false,pet:true});
  }



  vetbtn=()=>{
    _that.props.navigation.navigate('locationScreen',{home:false,service:true,more:false,pet:false});
  }

  shelter=()=>{
    _that.props.navigation.navigate('PetShelters',{home:false,service:true,more:false,pet:false});
  }

  groomer=()=>{
    _that.props.navigation.navigate('Petgroomers',{home:false,service:true,more:false,pet:false});
  }

  training=()=>{
    _that.props.navigation.navigate('PetTraining',{home:false,service:true,more:false,pet:false});
  }

  friendly=()=>{
    _that.props.navigation.navigate('AnimalFriendly',{home:false,service:true,more:false,pet:false});
  }




  render() {
    const headerProp = {
      title: 'WELCOME',
      screens: '',
    };

    return (

      <View style={styles.container}>

            <View style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
                <View style={styles.mainbox}>
                  <Image source={logo} style={styles.logo}/>

                  <View style={styles.centerbox}>
                        <TouchableOpacity onPress={this.groomer} style={[styles.box,styles.shadow]}>
                            <Image source={aa} style={styles.icon}/>
                            <Text style={styles.title}>Pet Groomers</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.training} style={[styles.box,styles.shadow]}>
                            <Image source={bb} style={styles.icon}/>
                            <Text style={styles.title}>Pet Training Centers</Text>
                        </TouchableOpacity>
                  </View>
                    <View style={styles.centerbox}>
                        <TouchableOpacity onPress={this.friendly} style={[styles.box,styles.shadow]}>
                            <Image source={cc} style={styles.icon}/>
                            <Text style={styles.title}>Animal Friendly Locations</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.vetbtn}  style={[styles.box,styles.shadow]}>
                            <Image source={dd} style={styles.icon}/>
                            <Text style={styles.title}>Veterinarians</Text>
                        </TouchableOpacity>
                    </View>
                      <View style={styles.centerbox}>
                        <TouchableOpacity onPress={this.shelter} style={[styles.box,styles.shadow]}>
                            <Image source={ee} style={styles.icon}/>
                            <Text style={styles.title}>Animal Shelters</Text>
                        </TouchableOpacity>
                        <TouchableOpacity  style={styles.box1}>

                        </TouchableOpacity>
                </View>


                </View>
            </View>

            <View style={{backgroundColor:'#c41a39',zIndex:9999,width:'100%',height:50,flexDirection:'row',justifyContent:'space-around',position:'absolute',bottom:0}}>
              <TouchableOpacity onPress={this.Homebtn}  style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.home?{opacity:1}:{opacity:0.5}]}>
                <Image source={home} style={styles.tab_icon}/>
                <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>Adopt</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.Petbtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.pet?{opacity:1}:{opacity:0.5}]}>
                <Image source={pet} style={styles.tab_icon}/>
                <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>My Pet</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.Servicebtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.service?{opacity:1}:{opacity:0.5}]}>
                <Image source={service} style={styles.tab_icon}/>
                <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>Services</Text>
              </TouchableOpacity>
              <TouchableOpacity  onPress={this.Morebtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.more?{opacity:1}:{opacity:0.5}]}>
                <Image source={more} style={styles.tab_icon}/>
                <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>More</Text>
              </TouchableOpacity>
            </View>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center',
    width:'100%',
    backgroundColor:'#fff',

  },
  content: {
      flex: 1,
      width:'100%',
      height:'100%',
      position:'relative',

  },
  box:{
    width:'46%',
    backgroundColor:'#f2f2f2',
    alignItems:'center',
    marginBottom:15,
    height:AppSizes.screen.height/5.5,
    //height:AppSizes.ResponsiveSize.height/2
    padding:10,
    borderRadius:15,
    textAlign:'center',
    position:'relative'
  },
  box1:{
    width:'46%',
    backgroundColor:'#fff',
    alignItems:'center',
    marginBottom:15,
    //height:AppSizes.ResponsiveSize.height/2
    padding:10,
    borderRadius:15,
    textAlign:'center',
    position:'relative'
  },

  mainbox:{
    width:'100%',
    alignItems:'center',

    marginBottom:80,
    paddingTop:AppSizes.ResponsiveSize.Padding(10)
  },
  shadow:{
    borderWidth:0,
    borderRadius: 5,
    justifyContent:'center',
    backgroundColor:'#f2f2f2',
    borderColor: '#999',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.5,
    shadowRadius: 3,
    elevation: 2,
    zIndex:0,
    //backgroundColor:'red',
 },
 logo:{
   width:AppSizes.screen.width/3,
   height:AppSizes.screen.height/5,
   marginBottom:AppSizes.ResponsiveSize.Padding(5)
 },
 icon:{
   width:50,
   height:50,
   marginBottom:10
 },
 title:{
   fontSize:AppSizes.ResponsiveSize.Sizes(15),
   color:'#000',
   fontWeight:'700',
   textAlign:'center',
   fontFamily:'Roboto-Bold'
 },
 centerbox:{
   width:'90%',
   flexDirection:'row',
   justifyContent:'space-around',
   alignItems:'center'
 },
 tab_icon:{
   width:28,
   height:28
 }

});
