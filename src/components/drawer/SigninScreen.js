import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput,Alert} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import CommonButton from '../common/CommonButton';
import SelectButton from '../common/SelectButton';
import * as commonFunctions from '../../utils/CommonFunctions';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

import { Fonts } from '../../utils/Fonts';

const background = require('../../themes/Images/background.png')
const logo = require('../../themes/Images/logo.png')
const facebook = require('../../themes/Images/facebook.png')
const google = require('../../themes/Images/googleplus.png')
const user = require('../../themes/Images/user.png')
const password = require('../../themes/Images/password.png')

let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      hidePassword:true,
      password:'',
      email:''
    },
    _that = this;
  }

  componentWillMount() {
      AsyncStorage.getItem('loggedIn').then((value) => {
            var loggedIn = JSON.parse(value);
            if(loggedIn){
              _that.props.navigation.navigate('HomeScreen',{home:true,service:false,more:false,pet:false});
            }
        })
    }

  validationAndApiParameter() {
        const { email,password,isVisible } = this.state
        var error=0

        if ((email.indexOf(' ') >= 0 || email.length <= 0)) {
          //this.setState({emailErrorMessage:'Please enter Email address!'});
          error=1;
          Alert.alert('Login Error','Please enter Email address!');
        }else if (!commonFunctions.validateEmail(email)) {
          error=1;
          Alert.alert('Login Error','Please enter valid Email address!');
        }
        else if ((password.indexOf(' ') >= 0 || password.length <= 0)) {
          error=1;
          Alert.alert('Login Error','Please enter password!');
        }
        if(error==0){

           var data = {
             email: email,
             password: password
           };

          console.log(data);

          _that.setState({
            isVisible: true
          });
          this.postToApiCalling('POST', 'login', Constant.URL_Signin, data);
        }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {

     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
          if ((!jsonRes) || (jsonRes.code == 0)) {
            console.log(jsonRes);
            Alert.alert("Error",jsonRes.message)
            _that.setState({ isVisible: false })
          }else {
            if (jsonRes.code == 1) {
                _that.apiSuccessfullResponse(apiKey, jsonRes)
            }
            else if (jsonRes.code==2) {
              Alert.alert("Login Error",jsonRes.message)
            }
            else if (jsonRes.code==3) {
              Alert.alert("Can’t Find Account",jsonRes.message)
            }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(()=>{
              Alert.alert("Server issue" + error);
          },200);
      });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
    if (apiKey == 'login') {
      console.log(jsonRes);
      AsyncStorage.setItem("loggedIn", JSON.stringify(true)).done();
      AsyncStorage.setItem('UserData', JSON.stringify(jsonRes.data));
      AsyncStorage.setItem('Animal_data',JSON.stringify(jsonRes.adopt_list))
      _that.props.navigation.navigate('HomeScreen',{home:true,service:false,more:false,pet:false});
    }
  }

  managePasswordVisibility = () =>
   {
     this.setState({ hidePassword: !this.state.hidePassword });
   }

  Signupbtn=()=>{
    const resetAction = NavigationActions.navigate({ routeName: 'SignUpScreen'})
    _that.props.navigation.dispatch(resetAction);
  }
  Homebtn=()=>{
    _that.validationAndApiParameter()
  }
  Forgotbtn=()=>{
    const resetAction = NavigationActions.navigate({ routeName: 'ForgotPassword'})
    _that.props.navigation.dispatch(resetAction);
  }

  render() {
    return (

      <View style={styles.container}>

          <ImageBackground source={background} style={{width: '100%', height: '100%'}}>
            <KeyboardAwareScrollView>
              <View style={styles.logosec}>

                  <View style={styles.logo}/>

                  <View style={styles.box}>

                  <View style={styles.inputbox}>
                      {/*<Image source={user} style={styles.iconinput}/>*/}
                        <TextInput style = {[styles.input,styles.shadow]}
                          ref="email"
                          placeholder = "Email"
                          placeholderTextColor = "#fff"
                          autoCapitalize = "none"
                          keyboardType={ 'email-address'}
                          onChangeText = {(email) =>this.setState({email})}/>
                  </View>
                  <View style={styles.passinputbox}>
                      <TextInput style = {[styles.pass_input,styles.input]}
                        type="password"
                        placeholder = "Password"
                        placeholderTextColor = "#fff"
                        ref="password"
                        autoCapitalize = "none"
                        secureTextEntry = { this.state.hidePassword }
                        onChangeText = {(password)=>this.setState({password})}/>
                        <TouchableOpacity activeOpacity = { 0.8 } style = { styles.visibilityBtn } onPress = { this.managePasswordVisibility }>
                           {this.state.hidePassword?
                             <Text style={{color:'#c41a39',fontWeight:'700',width:60}}>SHOW</Text>
                           :
                            <Text style={{color:'#c41a39',fontWeight:'700',width:60}}>HIDE</Text>
                           }
                        </TouchableOpacity>
                  </View>

                    <TouchableOpacity style={styles.btnblksign} onPress={this.Homebtn} activeOpacity={.6} >
                      <CommonButton label='Sign In'/>
                      </TouchableOpacity>

                      <TouchableOpacity onPress={this.Forgotbtn} style={styles.btnblk1} activeOpacity={.6} >
                            <SelectButton label='Forgot Password'/>
                        </TouchableOpacity>

                        </View>
<TouchableOpacity style={styles.btnblk} onPress={this.Signupbtn} activeOpacity={.6} >
    <View  style={{width:'100%',backgroundColor:'transparent', height:AppSizes.screen.width/7,alignItems:'center', justifyContent: 'center',paddingLeft:AppSizes.ResponsiveSize.Padding(1),paddingRight:AppSizes.ResponsiveSize.Padding(1),borderRadius:50}}>
        <Text style={styles.textbtn}>Create an Account</Text>
    </View>
</TouchableOpacity>
              </View>



  </KeyboardAwareScrollView>
          </ImageBackground>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center'
  },
  logosec:{
      height:AppSizes.screen.height/1,
      justifyContent:'center',
      alignItems:'center',
      position:'relative'
  },

  logo:{
    width:180,
    height:175,
    marginBottom:AppSizes.ResponsiveSize.Padding(5)
  },
  textbtn:{
    color:'#fff',
    fontSize:AppSizes.ResponsiveSize.Sizes(17),

  },
  textbtnn:{
    color:'#fff',
    fontSize:AppSizes.ResponsiveSize.Sizes(16),
    fontFamily:Fonts.Robotor
  },
  input: {
    marginBottom:AppSizes.ResponsiveSize.Padding(3),
     height: 58,
     backgroundColor:'transparent',
     borderRadius:50,
     paddingLeft:25,
     width:'100%',
     borderColor:'#fff',
     borderWidth:2,
     padding:10,
     color:'#fff',
     fontSize:AppSizes.ResponsiveSize.Sizes(18),
     fontFamily:Fonts.Robotor
  },
  border:{
    height:2,
    backgroundColor:'#fff',
    width:60,
    marginBottom:20
  },
  title:{
    color:'#fff',
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    marginBottom:10,
    fontFamily:Fonts.Robotor

  },
  box:{
    width:'90%',
    padding:0,
    alignItems:'center',
    marginLeft:'auto',
    marginRight:'auto',
    borderRadius:20,

  },
  inputbox:{
    width:'100%',
    position:'relative',
    marginBottom:AppSizes.ResponsiveSize.Padding(3)
  },

  btnblk:{
    width:'100%',
    position:'absolute',
    bottom:20,
},
btnblksign:{
    width:'100%',
  marginTop:AppSizes.ResponsiveSize.Padding(2)
},
bottomblk:{
  justifyContent:'flex-end',
  height:AppSizes.screen.height/6,
  flexDirection:'row',
  width:'90%',
  padding:0,
  alignItems:'center',
  marginLeft:'auto',
  marginRight:'auto',
  position:'absolute',
  bottom:10
},
icon:{
  height:50,
  width:50,
  marginLeft:AppSizes.ResponsiveSize.Padding(5)
},
iconinput:{
  height:50,
  width:50,
  position:'absolute',
  top:8,
  left:8
},
shadow:{
  justifyContent:'center',
  backgroundColor:'transparent',
  borderColor: '#fff',
  shadowColor: '#000',
  shadowOffset: { width: 2, height: 2 },
  shadowOpacity: 0.5,
  shadowRadius: 3,
  elevation: 2,
  zIndex:0,
},
pass_input:{
  fontSize: AppSizes.ResponsiveSize.Sizes(18),
  alignSelf: 'stretch',
  height: 45,
  paddingRight: 45,
  paddingLeft: 8,
  paddingVertical: 0,
  borderBottomColor:'#a3a3a3',
  borderBottomWidth:2,
  marginBottom:15
},
passinputbox:{
 position: 'relative',
 alignSelf: 'stretch',
 justifyContent: 'center',
 height: 45,
 marginBottom:15
},
visibilityBtn:{
  position: 'absolute',
  right: 20,
  //backgroundColor:'red',
  height: '100%',
  justifyContent:'center',
  top:-5,
  width: 35,
},
});
