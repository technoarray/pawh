import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Dimensions,Platform,ImageBackground,ScrollView,TextInput,Alert,Modal} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import CommonButton from '../common/CommonButton';
import SelectButton from '../common/SelectButton';
import Header from '../common/Headerwithback'
import { Icon } from 'react-native-elements'
import BottomTabs from '../common/BottomTabs'
import MapView, { Marker, ProviderPropType } from 'react-native-maps';
import Geocoder from 'react-native-geocoder';
import PolyLine from '@mapbox/polyline';
import getDirections from 'react-native-google-maps-directions'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

import { Fonts } from '../../utils/Fonts';

const location = require('../../themes/Images/directions-icon.png')
const property = require('../../themes/Images/property.jpg')

var home=require('../../themes/Images/2.png')
var pet=require('../../themes/Images/1.png')
var service=require('../../themes/Images/3.png')
var more=require('../../themes/Images/4.png')
var back=require('../../themes/Images/back_modal.png')

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE =25.2048;
const LONGITUDE = 55.2708;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      veterinarians:[],
      home:this.props.navigation.state.params.home,
      service:this.props.navigation.state.params.service,
      more:this.props.navigation.state.params.more,
      pet:this.props.navigation.state.params.pet,
      Modal_Visibility:false,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      concat: null,
      latitude: null,
      longitude: null,
      error: null,
      concat: null,
      coords:[],
      x: 'false',
    },
    _that = this;
  }

  Singlebtn=()=>{
    _that.props.navigation.navigate('SingleScreen',{home:this.state.home,service:this.state.service,more:this.state.more,pet:this.state.pet});
  }

  Homebtn=()=>{
    this.setState({
      home:true,
      service:false,
      more:false,
      pet:false
    })
    _that.props.navigation.navigate('HomeScreen',{home:true,service:false,more:false,pet:false});
  }
  Servicebtn=()=>{
    this.setState({
      home:false,
      service:true,
      more:false,
      pet:false
    })
    _that.props.navigation.navigate('ServicesScreen',{home:false,service:true,more:false,pet:false});
  }

  Morebtn=()=>{
    this.setState({
      home:false,
      service:false,
      more:true,
      pet:false
    })
    _that.props.navigation.navigate('SettingScreen',{home:false,service:false,more:true,pet:false});
  }

  Petbtn=()=>{
    this.setState({
      home:false,
      service:false,
      more:false,
      pet:true
    })
    _that.props.navigation.navigate('Pet_detail_form',{home:false,service:false,more:false,pet:true});
  }

  componentDidMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
          _that.validationAndApiParameter(uid)
        }
    })
  }

  componentWillMount(){
    try {
    Geocoder.fallbackToGoogle('AIzaSyBX6rKXe6Jsk6ZynShEZiNfDfyhZWgmXsQ');
     navigator.geolocation.getCurrentPosition(
      (position) => {
        var region = {
          lat: position.coords.latitude,
          lng:  position.coords.longitude,
        };
        //_that.validationAndApiParameter('ServiceList',uid,region.lat,region.lng)
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            accuracy: position.coords.accuracy
          }
        });
        this.setState({
           latitude: position.coords.latitude,
           longitude: position.coords.longitude,
           error: null,
         });
        Geocoder.geocodePosition(region).then(res => {
          //console.log(res)
          var address=res[0].formattedAddress
          _that.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            address: address,
          });
        })
        this.watchID = navigator.geolocation.watchPosition((position) => {
          const newRegion = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            accuracy: position.coords.accuracy
          }
          this.setState({newRegion});
        })
      },
      (error) => console.log('error '+error.message),
      {enableHighAccuracy: false, timeout: 50000, maximumAge: 10000}
    );
    }
    catch(err) {
        console.log(err);
    }
  }

  validationAndApiParameter(uid) {
     var data = {
       uid:uid,
     };
    this.postToApiCalling('POST', 'signup', Constant.URL_petVeterinarians, data);
  }

  postToApiCalling(method, apiKey, apiUrl, data) {

     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
          if ((!jsonRes) || (jsonRes.code == 0)) {
            console.log(jsonRes);
            Alert.alert("Error",jsonRes.message)
            _that.setState({ isVisible: false })
          }else {
            if (jsonRes.code == 1) {
                _that.apiSuccessfullResponse(apiKey, jsonRes)
            }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(()=>{
              Alert.alert("Server issue" + error);
          },200);
      });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
      if (apiKey == 'signup') {
        console.log(jsonRes)
        this.setState({
          veterinarians:jsonRes.veterinarians,
          lat:Number(jsonRes.veterinarians[0].lat),
          lgt:Number(jsonRes.veterinarians[0].lon),
          cordLatitude:Number(jsonRes.veterinarians[0].lat),
          cordLongitude:Number(jsonRes.veterinarians[0].lon),
        })
      }
  }

  handleGetDirections(lat,lng){
    const data = {
       source: {
        latitude: this.state.latitude,
        longitude:this.state.longitude
      },
      destination: {
        latitude: Number(lat),
        longitude: Number(lng)
      },
      params: [
        {
          key: "travelmode",
          value: "driving"        // may be "walking", "bicycling" or "transit" as well
        },
        {
          key: "dir_action",
          value: "navigate"       // this instantly initializes navigation using the given travel mode
        }
      ]
    }

    getDirections(data)
  }

  render() {
    const headerProp = {
      title: 'Veterinarians',
      screens: '',
    };

    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>

          <ScrollView style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
              <View style={styles.mainbox}>
                {this.state.veterinarians.map(data => (
                  <TouchableOpacity  style={[styles.box,styles.shadow]} key={data.id}>
                      <View style={styles.imgbox}>
                          <Image source={{uri:Constant.image_path4 + data.image}} style={styles.productimg}/>
                      </View>
                      <View style={styles.contentbox}>
                        <Text style={styles.price}>{data.name}</Text>
                        <Text style={styles.gender}><Text style={styles.bold}>Contact</Text> - {data.number}</Text>
                        <Text style={styles.location}><Text style={styles.bold}>Location</Text> - {data.address}</Text>
                      </View>
                      <View style={styles.favBox}>
                        <TouchableOpacity onPress={()=> this.handleGetDirections(data.lat,data.lon)} style={styles.favicon}>
                            <Image source={location} style={styles.icon}/>
                        </TouchableOpacity>
                      </View>
                  </TouchableOpacity>
                ))}
              </View>
          </ScrollView>

          <View style={{backgroundColor:'#c41a39',zIndex:9999,width:'100%',height:50,flexDirection:'row',justifyContent:'space-around',position:'absolute',bottom:0}}>
            <TouchableOpacity onPress={this.Homebtn}  style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.home?{opacity:1}:{opacity:0.5}]}>
              <Image source={home} style={styles.bottom_icon}/>
              <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>Adopt</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.Petbtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.pet?{opacity:1}:{opacity:0.5}]}>
              <Image source={pet} style={styles.bottom_icon}/>
              <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>My Pet</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.Servicebtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.service?{opacity:1}:{opacity:0.5}]}>
              <Image source={service} style={styles.bottom_icon}/>
              <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>Services</Text>
            </TouchableOpacity>
            <TouchableOpacity  onPress={this.Morebtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.more?{opacity:1}:{opacity:0.5}]}>
              <Image source={more} style={styles.bottom_icon}/>
              <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>More</Text>
            </TouchableOpacity>
          </View>
      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center',
    width:'100%',
    backgroundColor:'#fff'
  },
  content: {
      flex: 1,
      width:'100%',
      height:'100%',
      position:'relative'
  },
  box:{
    width:'90%',
    //backgroundColor:'green',
    alignItems:'center',
    marginBottom:15,
    flexDirection:'row',
    //height:AppSizes.ResponsiveSize.height/2
    padding:10,
    borderRadius:15,
    textAlign:'center',
    position:'relative'
  },
  icon1:{
    width:AppSizes.screen.width/20,
      height:AppSizes.screen.height/40,
  },

  price:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    marginBottom:AppSizes.ResponsiveSize.Padding(1),
    color:'#000',
    fontWeight:'700',
      fontFamily:'Roboto-Bold'
  },
  gender:{
    fontSize:AppSizes.ResponsiveSize.Sizes(10),
    color:'#333',
marginBottom:AppSizes.ResponsiveSize.Padding(.50),
  fontFamily:Fonts.Robotor
    },
    bold:{
fontWeight:'700',
  fontFamily:'Roboto-Bold'
    },
    breed:{
      fontSize:AppSizes.ResponsiveSize.Sizes(10),
      color:'#333',
marginBottom:AppSizes.ResponsiveSize.Padding(.50),
  fontFamily:Fonts.Robotor
      },
      location:{
        fontSize:AppSizes.ResponsiveSize.Sizes(10),
        color:'#333',
  marginBottom:AppSizes.ResponsiveSize.Padding(.50),
    fontFamily:Fonts.Robotor
        },
  imgbox:{
    width:'30%',

  height:100,

  },
  contentbox:{
    width:'50%',
    paddingLeft:10
  },
  productimg:{
    width:'100%',
    height:'100%',
    borderRadius:5
  },
  mainbox:{
    width:'100%',
    alignItems:'center',
    paddingTop:AppSizes.ResponsiveSize.Padding(3),
    marginBottom:80
  },
  shadow:{
    borderWidth:0,
    borderRadius: 5,
    justifyContent:'center',
    backgroundColor:'#f2f2f2',
    borderColor: '#999',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.5,
    shadowRadius: 3,
    elevation: 2,
    zIndex:0,
    //backgroundColor:'red',
 },
 favBox:{
   width:'20%',
 },
 favicon:{
   //position:'absolute',
  // right:10,
   top:0,
   backgroundColor:'#c41a39',
   borderRadius:50,
   padding:13,
   width:50,
   height:50,
   alignSelf:'flex-end'
 },
icon:{
  width:25,
  height:25
},
bottom_icon:{
  width:28,
  height:28
},
map: {
    ...StyleSheet.absoluteFillObject,
  },
  back:{
    height:30,
    width:30
  }


});
