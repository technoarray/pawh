import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput,Alert} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import CommonButton from '../common/CommonButton';
import SelectButton from '../common/SelectButton';
import * as commonFunctions from '../../utils/CommonFunctions';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

import { Fonts } from '../../utils/Fonts';

const background = require('../../themes/Images/background.png')
const logo = require('../../themes/Images/logo.png')
const facebook = require('../../themes/Images/facebook.png')
const google = require('../../themes/Images/googleplus.png')
const user = require('../../themes/Images/user.png')
const password = require('../../themes/Images/password.png')
const email = require('../../themes/Images/email.png')

let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      hidePassword:true,
      username:'',
      password:'',
      email:''
    },
    _that = this;
  }

  validationAndApiParameter() {
        const { username,email,password,isVisible } = this.state
        var error=0

        if ((username.indexOf(' ') >= 0 || username.length <= 0)) {
          //this.setState({emailErrorMessage:'Please enter Email address!'});
          error=1;
          Alert.alert('Login Error','Please enter Username!');
        }
        else if ((email.indexOf(' ') >= 0 || email.length <= 0)) {
          //this.setState({emailErrorMessage:'Please enter Email address!'});
          error=1;
          Alert.alert('Login Error','Please enter Email address!');
        }else if (!commonFunctions.validateEmail(email)) {
          error=1;
          Alert.alert('Login Error','Please enter valid Email address!');
        }
        else if ((password.indexOf(' ') >= 0 || password.length <= 0)) {
          error=1;
          Alert.alert('Login Error','Please enter password!');
        }
        if(error==0){

           var data = {
             username:username,
             email: email,
             password: password
           };

          console.log(data);

          _that.setState({
            isVisible: true
          });
          this.postToApiCalling('POST', 'signup', Constant.URL_Signup, data);
        }
  }

  postToApiCalling(method, apiKey, apiUrl, data) {

     new Promise(function(resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
        console.log(jsonRes);
          if ((!jsonRes) || (jsonRes.code == 0)) {
            console.log(jsonRes);
            Alert.alert("Sorry...",jsonRes.message)
            _that.setState({ isVisible: false })
          }else {
            if (jsonRes.code == 1) {
                _that.apiSuccessfullResponse(apiKey, jsonRes)
            }
          }
      }).catch((error) => {
          console.log("ERROR" + error);
          _that.setState({ isVisible: false })
          setTimeout(()=>{
              Alert.alert("Server issue" + error);
          },200);
      });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
      if (apiKey == 'signup') {
        Alert.alert(
          'Paw-some!',
          'Your account has been created successfully.',
          [
            {
              text: 'Ok',
              onPress: () => _that.props.navigation.navigate('SigninScreen'),
              style: 'cancel',
            },
          ],
          {cancelable: false},
        );
        // console.log(jsonRes)
        // AsyncStorage.setItem("loggedIn", JSON.stringify(true)).done();
        // AsyncStorage.setItem('UserData', JSON.stringify(jsonRes.data));
        // AsyncStorage.setItem('Animal_data',JSON.stringify(jsonRes.adopt_list))
        // _that.props.navigation.navigate('HomeScreen');
      }
  }


    managePasswordVisibility = () =>
     {
       this.setState({ hidePassword: !this.state.hidePassword });
     }

    Signupbtn=()=>{
      const resetAction = NavigationActions.navigate({ routeName: 'SignUpScreen'})
      _that.props.navigation.dispatch(resetAction);
    }

    Loginbtn=()=>{
      const resetAction = NavigationActions.navigate({ routeName: 'SigninScreen'})
      _that.props.navigation.dispatch(resetAction);
    }

    Homebtn=()=>{
      _that.validationAndApiParameter()
    }

  render() {
    return (
      <View style={styles.container}>

            <KeyboardAwareScrollView style={styles.fullbox}>
              <View style={styles.logosec}>
                  <View style={styles.logoblk}>
                      <Image source={logo} style={styles.logo}/>
                  </View>
                  <View style={styles.box}>

                  <View style={styles.inputbox}>
                        <TextInput style = {styles.input}
                          placeholder = "Username"
                          placeholderTextColor = "#a3a3a3"
                          ref="username"
                          keyboardType={ 'default'}
                          onChangeText = {(username)=>this.setState({username})}/>
                  </View>
                  <View style={styles.inputbox}>
                      <TextInput style = {styles.input}
                        placeholder = "Email Address"
                        placeholderTextColor = "#a3a3a3"
                        ref="email"
                        keyboardType={ 'email-address'}
                        autoCapitalize = "none"
                        onChangeText = {(email)=> this.setState({email})}/>
                  </View>
                  <View style={styles.passinputbox}>
                      <TextInput style = {[styles.pass_input,styles.input]}
                        type="password"
                        placeholder = "Password"
                        placeholderTextColor = "#a3a3a3"
                        ref="password"
                        autoCapitalize = "none"
                        secureTextEntry = { this.state.hidePassword }
                        onChangeText = {(password)=>this.setState({password})}/>
                        <TouchableOpacity activeOpacity = { 0.8 } style = { styles.visibilityBtn } onPress = { this.managePasswordVisibility }>
                           {this.state.hidePassword?
                              <Text style={{color:'#c41a39',fontWeight:'700',width:60}}>SHOW</Text>
                           :
                              <Text style={{color:'#c41a39',fontWeight:'700',width:60}}>HIDE</Text>
                           }
                        </TouchableOpacity>
                  </View>

                    <TouchableOpacity style={styles.btnblksign} onPress={this.Homebtn} activeOpacity={.6} >
                      <CommonButton label='Create Account'/>
                      </TouchableOpacity>

                </View>
                <TouchableOpacity style={[styles.btnblksign,styles.botbtn]} onPress={this.Loginbtn} activeOpacity={.6} >
                    <View  style={{width:'100%', height:AppSizes.screen.width/10,alignItems:'center', justifyContent: 'center',paddingLeft:AppSizes.ResponsiveSize.Padding(1),paddingRight:AppSizes.ResponsiveSize.Padding(1)}}>
                        <Text style={styles.textbtn}>Already have an account? <Text style={styles.bold}>Sign In</Text></Text>
                    </View>
                  </TouchableOpacity>
              </View>
              </KeyboardAwareScrollView>

    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#fff',
    width:'100%'
  },
  logosec:{
    height:AppSizes.screen.height/1,
    width:'100%',
    alignItems:'center',
    position:'relative',
    paddingTop:AppSizes.ResponsiveSize.Padding(3)
  },
  textbtn:{
    color:'#a3a3a3',
    fontSize:AppSizes.ResponsiveSize.Sizes(17),
    fontFamily:Fonts.Robotor
  },
  bold:{
    fontWeight:'600',
    color:'#c41a39',

  },
  textbtnn:{
    color:'#fff',
    fontSize:AppSizes.ResponsiveSize.Sizes(13),
  },
  input: {
     marginBottom:AppSizes.ResponsiveSize.Padding(3),
     height: 58,
     backgroundColor:'transparent',
     paddingLeft:0,
     width:'100%',
     borderBottomColor:'#a3a3a3',
     borderBottomWidth:2,
     padding:10,
     color:'#000',
     fontFamily:Fonts.Robotor,
     fontSize:AppSizes.ResponsiveSize.Sizes(18)
  },
  pass_input:{
    fontSize: AppSizes.ResponsiveSize.Sizes(18),
    alignSelf: 'stretch',
    height: 45,
    paddingRight: 45,
    paddingLeft: 8,
    paddingVertical: 0,
    borderBottomColor:'#a3a3a3',
    borderBottomWidth:2,
  },
  border:{
    height:2,
    backgroundColor:'#fff',
    width:60,
    marginBottom:20
  },
  title:{
    color:'#fff',
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    marginBottom:10,
    fontFamily:Fonts.Robotor
  },
  visibilityBtn:{
    position: 'absolute',
    right: 20,
    height: 40,
    bottom:5,
    width: 35,
  },
  box:{
    width:'90%',
    padding:0,
    alignItems:'center',
    marginLeft:'auto',
    marginRight:'auto',
    borderRadius:20,
  },
  inputbox:{
    width:'100%',
    position:'relative',
    marginBottom:AppSizes.ResponsiveSize.Padding(10)
  },
  passinputbox:{
   position: 'relative',
   alignSelf: 'stretch',
   justifyContent: 'center'
  },
  btnblk:{
    width:'100%',
  },
  btnblksign:{
      width:'100%',
    marginTop:AppSizes.ResponsiveSize.Padding(2)
  },
  bottomblk:{
    justifyContent:'flex-end',
    height:AppSizes.screen.height/6,
    flexDirection:'row',
    width:'90%',
    padding:0,
    alignItems:'center',
    marginLeft:'auto',
    marginRight:'auto',
  },
  icon:{
    height:50,
    width:50,
    marginLeft:AppSizes.ResponsiveSize.Padding(5)
  },
  iconinput:{
    height:50,
    width:50,
    position:'absolute',
    top:8,
    left:8
  },
  border:{
    height:2,
    backgroundColor:'#fff',
    width:30,
    marginBottom:AppSizes.ResponsiveSize.Padding(1),
    marginBottom:30
  },
  title:{
    color:'#fff',
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    marginBottom:10,
    fontWeight:'600',
    fontFamily:Fonts.Robotor
  },
  titlesec:{
    height:AppSizes.screen.height/4,
    width:'100%',
    alignItems:'center',
    justifyContent:'flex-end'
  },
  logo:{
    width:180,
    height:175,
    marginBottom:AppSizes.ResponsiveSize.Padding(5)
  },
  fullbox:{
    width:'90%',
  },
  botbtn:{
    position:'absolute',
    bottom:30
  },
  logoblk:{
    height:AppSizes.screen.height/3,
    width:'100%',
    //backgroundColor:'gray',
    alignItems:'center',
    justifyContent:'center'
  },

});
