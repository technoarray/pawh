import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput,Alert} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import CommonButton from '../common/CommonButton';
import SelectButton from '../common/SelectButton';
import Header from '../common/Headerwithbackhide'
import { Icon } from 'react-native-elements'
import BottomTabs from '../common/BottomTabs'
var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

import { Fonts } from '../../utils/Fonts';

import Swiper from 'react-native-swiper-animated';

const property = require('../../themes/Images/property.jpg')
var back = require( '../../themes/Images/backwhite.png')

var home=require('../../themes/Images/2.png')
var pet=require('../../themes/Images/1.png')
var service=require('../../themes/Images/3.png')
var more=require('../../themes/Images/4.png')
var data

let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      home:this.props.navigation.state.params.home,
      service:this.props.navigation.state.params.service,
      more:this.props.navigation.state.params.more,
      pet:this.props.navigation.state.params.pet,
      pet_id:this.props.navigation.state.params.pet_id,
      requested:false
    },
    _that = this;
  }

    RequestBtn=()=>{
      const resetAction = NavigationActions.navigate({ routeName: 'Request_adoption'})
      _that.props.navigation.navigate('Request_adoption',{aid:this.state.pet_id});
    }
    Backbtn=()=>{
      data={}
      _that.props.navigation.goBack(null);
    }

    componentWillMount(){
      AsyncStorage.getItem('UserData').then((UserData) => {
        var data = JSON.parse(UserData);
          uid=data.id
          if(uid != ''){
            _that.setState({
              uid:uid,
            })
            _that.validationAndApiParameter('getDetails',uid)
          }
      })
    }

    validationAndApiParameter(apiKey,uid) {
      if(apiKey=='getDetails'){
        var data={
          uid:uid,
          id:this.props.navigation.state.params.pet_id
        }
        console.log(data);
        this.postToApiCalling('POST', 'getDetails', Constant.URL_showDetails, data);
      }
    }

    postToApiCalling(method, apiKey, apiUrl, data) {

       new Promise(function(resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
                resolve(WebServices.callWebService_GET(apiUrl, data));
            }
        }).then((jsonRes) => {
            console.log(jsonRes);
            if ((!jsonRes) || (jsonRes.code == 0)) {
              console.log(jsonRes);
              Alert.alert("Error",jsonRes.message)
              _that.setState({ isVisible: false })
            }else {
              if (jsonRes.code == 1) {
                  _that.apiSuccessfullResponse(apiKey, jsonRes)
              }
            }
        }).catch((error) => {
            console.log("ERROR" + error);
            _that.setState({ isVisible: false })
            setTimeout(()=>{
                Alert.alert("Server issue" + error);
            },200);
        });
    }

    apiSuccessfullResponse(apiKey, jsonRes) {

      if(apiKey=='getDetails'){
        console.log(jsonRes);
        data=jsonRes.data
        this.setState({petDetail:jsonRes.data})
        this.setState({requested:jsonRes.requested})
        console.log(jsonRes);
      }
    }

    Homebtn=()=>{
      this.setState({
        home:true,
        service:false,
        more:false,
        pet:false
      })
      _that.props.navigation.navigate('HomeScreen',{home:true,service:false,more:false,pet:false});
    }
    Servicebtn=()=>{
      this.setState({
        home:false,
        service:true,
        more:false,
        pet:false
      })
      _that.props.navigation.navigate('ServicesScreen',{home:false,service:true,more:false,pet:false});
    }

    Morebtn=()=>{
      this.setState({
        home:false,
        service:false,
        more:true,
        pet:false
      })
      _that.props.navigation.navigate('SettingScreen',{home:false,service:false,more:true,pet:false});
    }
    Petbtn=()=>{
      this.setState({
        home:false,
        service:false,
        more:false,
        pet:true
      })
      _that.props.navigation.navigate('Pet_detail_form',{home:false,service:false,more:false,pet:true});
    }

  render() {
    const headerProp = {
      title: 'WELCOME',
      screens: '',
    };

    return (

      <View style={styles.container}>

        <ScrollView style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
        {data?
          <View style={styles.boxmain}>
              <View style={styles.boximg}>
                  <TouchableOpacity onPress={this.Backbtn} style={styles.onlybtn}>
                        <Image style={styles.image} source={back}/>
                  </TouchableOpacity>
                 <Image source={{uri: Constant.image_path+data.image}} style={styles.productimg}/>
              </View>

              <View style={styles.newstext}>
                    <View style={styles.pBox}>
                      <Text style={styles.price}>{data.name}</Text>
                    </View>


                    <View style={styles.buttonbox}>
                      <View style={styles.Viewbox}>
                          {data.pet_breed==''?<Text style={styles.breed}><Text style={styles.bold}>Breed</Text>:N/A</Text>:<Text style={styles.breed}><Text style={styles.bold}>Breed</Text>: {data.pet_breed}</Text>}
                          {data.gender==''?<Text style={styles.location}><Text style={styles.bold}>Gender</Text>:N/A</Text>:<Text style={styles.location}><Text style={styles.bold}>Gender</Text>: {data.gender}</Text>}
                          {data.age==''?<Text style={styles.location}><Text style={styles.bold}>Age</Text>:N/A</Text>:<Text style={styles.location}><Text style={styles.bold}>Age</Text>: {data.age}</Text>}
                          {data.color==''?<Text style={styles.location}><Text style={styles.bold}>Color</Text>:N/A</Text>:<Text style={styles.location}><Text style={styles.bold}>Color</Text>: {data.color}</Text>}
                          {data.address==''?<Text style={styles.location}><Text style={styles.bold}>Location</Text>:N/A</Text>:<Text style={styles.location}><Text style={styles.bold}>Location</Text>: {data.address}</Text>}
                      </View>
                      {this.state.requested=='true'?
                        <View  style={styles.button}>
                            <Text style={styles.buttontext}>Already Requested</Text>
                        </View>
                      :
                        <TouchableOpacity onPress={this.RequestBtn} style={styles.button}>
                            <Text style={styles.buttontext}>Request Adoption</Text>
                        </TouchableOpacity>
                      }
                    </View>

                    <Text style={styles.contenttext}>
                        {data.descp}
                    </Text>

                    <Text style={styles.location}><Text style={styles.bold}>Date posted</Text>: {data.created_date}</Text>
              </View>


          </View>
        :
          null
        }
        </ScrollView>

          <View style={{backgroundColor:'#c41a39',zIndex:9999,width:'100%',height:50,flexDirection:'row',justifyContent:'space-around',position:'absolute',bottom:0}}>
            <TouchableOpacity onPress={this.Homebtn}  style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.home?{opacity:1}:{opacity:0.5}]}>
              <Image source={home} style={styles.bottom_icon}/>
              <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>Adopt</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.Petbtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.pet?{opacity:1}:{opacity:0.5}]}>
              <Image source={pet} style={styles.bottom_icon}/>
              <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>My Pet</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.Servicebtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.service?{opacity:1}:{opacity:0.5}]}>
              <Image source={service} style={styles.bottom_icon}/>
              <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>Services</Text>
            </TouchableOpacity>
            <TouchableOpacity  onPress={this.Morebtn} style={[{height:'100%',justifyContent:'center',alignItems:'center'},this.state.more?{opacity:1}:{opacity:0.5}]}>
              <Image source={more} style={styles.bottom_icon}/>
              <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>More</Text>
            </TouchableOpacity>
          </View>

      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center',
    width:'100%',
    backgroundColor:'#fff'
  },
  boxmain:{
      width:'100%',
        alignItems:'center',
        marginBottom:60,
        position:'relative'
  },
    box:{
      width:'90%',
      backgroundColor:'green',
      alignItems:'center',
      marginBottom:10,
      flexDirection:'row',
      //height:AppSizes.ResponsiveSize.height/2
      padding:8,
      textAlign:'center',
      marginTop:-60
    },

    price:{
      fontSize:AppSizes.ResponsiveSize.Sizes(25),
      marginBottom:AppSizes.ResponsiveSize.Padding(2),
      color:'#000',
      fontWeight:'700',
      width:'100%',
fontFamily:'Roboto-Bold'
    },
Viewbox:{
  width:'50%'
},
    gender:{
      fontSize:AppSizes.ResponsiveSize.Sizes(11),
      color:'#333',
  marginBottom:AppSizes.ResponsiveSize.Padding(.50),
fontFamily:Fonts.Robotor
      },
      bold:{
  fontWeight:'300',
color:'#a3a3a3',
fontFamily:Fonts.Robotor
      },

      breed:{
        fontSize:AppSizes.ResponsiveSize.Sizes(15),
        color:'#333',
  marginBottom:AppSizes.ResponsiveSize.Padding(1),
  fontFamily:Fonts.Robotor
        },

        location:{
          fontSize:AppSizes.ResponsiveSize.Sizes(15),
          color:'#333',
    marginBottom:AppSizes.ResponsiveSize.Padding(.50),
    fontFamily:Fonts.Robotor
          },

    decription:{
      fontSize:AppSizes.ResponsiveSize.Sizes(13),
      color:'#333',
    fontWeight:'300',
fontFamily:Fonts.Robotor
      },
      decriptionsmall:{
        fontSize:AppSizes.ResponsiveSize.Sizes(15),
        color:'#333',
        fontWeight:'300',
    marginTop:AppSizes.ResponsiveSize.Padding(1),
    fontFamily:Fonts.Robotor
        },

    imgbox:{
      width:'30%',
    height:100,
    },
    contentbox:{
      width:'100%',
      padding:10
    },
    productimg:{
      width:'100%',
      height:'100%',
      borderRadius:10
    },

    shadow:{
      borderWidth:0,
      borderRadius: 5,
      justifyContent:'center',
      backgroundColor:'#fff',
      borderColor: '#999',
      borderBottomWidth: 0,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 0 },
      shadowOpacity: 0.5,
      shadowRadius: 3,
      elevation: 2,
      zIndex:0
      //backgroundColor:'red',
   },
     content: {
         flex: 1,
         backgroundColor: '#ffffff',
         height:'100%',
         width:'100%',

     },

     productimg:{
       height:AppSizes.screen.height/2.8,
       width:'100%',
       marginBottom:20
     },
     pBox:{
       flexDirection:'row',

     },
     iconbox:{
       flexDirection:'row',
       justifyContent:'space-around'
     },
     boxicon:{
       margin:AppSizes.ResponsiveSize.Padding(2),

     },
     image: {
       height:25,
       width:25
     },

     newstext:{
       width:'90%',

     },
     title:{
       fontSize:AppSizes.ResponsiveSize.Sizes(16),
       fontWeight:'700',
       color:'#333',
        marginTop:5,
       marginBottom:5,
       fontFamily:'Roboto-Bold'
     },
     contenttext:{
        width:'100%',
        marginBottom:10,
        fontSize:AppSizes.ResponsiveSize.Sizes(15),
        fontWeight:'300',
        color:'#333',
        lineHeight: AppSizes.ResponsiveSize.Sizes(13 * 1.50),
        fontFamily:Fonts.Robotor
     },
     buttonbox:{
       paddingTop:AppSizes.ResponsiveSize.Padding(1),
       marginTop:5,
       flexDirection:'row',
       marginBottom:20
     },
     button:{
       backgroundColor:'#c41a39',
       borderRadius:50,
       width:'50%',
       height:50,
       justifyContent:'center',
       alignItems:'center'
     },
     buttontext:{
       color:'#fff',
       fontSize:AppSizes.ResponsiveSize.Sizes(14),
       fontFamily:Fonts.Robotor
     },
     boximg:{
       width:'100%',
       position:'relative'
     },
     onlybtn:{
       position:'absolute',
       left:10,
       top:40,
       zIndex:111
     },
     bottom_icon:{
       width:28,
       height:28
     }

});
