import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput,Picker,Alert} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import CommonButton from '../common/CommonButton';
import SelectButton from '../common/SelectButton';
import Header from '../common/Headerwithback'
import { Icon } from 'react-native-elements'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import CheckBox from 'react-native-check-box'
import SelectInput from '@tele2/react-native-select-input';
import ImagePicker from 'react-native-image-picker';

import { Fonts } from '../../utils/Fonts';


var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

import Swiper from 'react-native-swiper-animated';

const property = require('../../themes/Images/property.jpg')
const uploadimg = require('../../themes/Images/image-upload.png')

let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      avatarSource:null,
      name:this.props.navigation.state.params.data.name,
      pet_breed:this.props.navigation.state.params.data.breed,
      color:this.props.navigation.state.params.data.color,
      age:this.props.navigation.state.params.data.age,
      weight:this.props.navigation.state.params.data.weight,
      height:this.props.navigation.state.params.data.height,
      pet_type:this.props.navigation.state.params.data.pet_type,
      gender:this.props.navigation.state.params.data.gender,
      status:this.props.navigation.state.params.data.status,
      data:this.props.navigation.state.params.data,
    },
    _that = this;
  }

  componentWillMount(){
    console.log(this.props.navigation.state.params.data);
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
        }
    })
  }
    selectPhotoTapped() {
      const options = {
        quality: 1.0,
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true,
        },
      };
        ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);

        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          const source = { uri: response.uri };

          this.setState({
            avatarSource: source,
          });
        }
      });
     }

     validationAndApiParameter(apiKey,uid) {
       if(apiKey=='Save'){
         const { uid,name,pet_type,pet_breed,color,age,gender,status,weight,height ,avatarSource} = this.state
         var error=0

         if(name==''){
           error=1
           Alert.alert('Input Error!','Please enter name')
         }

         if(error==0){
           // var myData=JSON.stringify([{var_name:'name',value:name},{var_name:'pet_type',value:pet_type},{var_name:'breed',value:pet_breed},{var_name:'color',value:color},{var_name:'age',value:age},{var_name:'gender',value:gender},{var_name:'status',value:status},{var_name:'weight',value:weight},{var_name:'height',value:height}])
           // var data = {
           //   uid:uid,
           //   id:this.props.navigation.state.params.data.id,
           //   variable:myData
           // };
           const data = new FormData();
           data.append('method','editMypet')
           data.append('uid', uid);
           data.append('name', name);
           data.append('pet_type', pet_type);
           data.append('breed', pet_breed);
           data.append('color', color);
           data.append('age', age);
           data.append('gender', gender);
           data.append('status', status);
           data.append('weight', weight);
           data.append('height', height);
           if(avatarSource!=null){data.append('image', {
             uri:  avatarSource.uri,
             type: 'image/jpeg', // or photo.type
             name: 'petImage.jpg'
           })}
           console.log(data);
           this.postToApiCalling('POST', 'Save', Constant.New_BASE, data);
         }
       }
     }

     postToApiCalling(method, apiKey, apiUrl, data) {
       console.log('hi');
        new Promise(function(resolve, reject) {
             if (method == 'POST') {
                 resolve(WebServices.callWebServices(apiUrl, data));
             } else {
                 resolve(WebServices.callWebService_GET(apiUrl, data));
             }
         }).then((responseJson) => {
           console.log(responseJson);
             if ((!responseJson) || (responseJson.code == 0)) {
               console.log(responseJson);
               Alert.alert("Error",responseJson.message)
               _that.setState({ isVisible: false })
             }else {
               if (responseJson.code == 1) {
                 _that.props.navigation.navigate('Pet_detail',{home:false,service:false,more:false,pet:true})
               }
             }
         }).catch((error) => {
             console.log("ERROR" + error);
             _that.setState({ isVisible: false })
             setTimeout(()=>{
                 Alert.alert("Server issue" + error);
             },200);
         });
     }

  PetBtn=()=>{
    const resetAction = NavigationActions.navigate({ routeName: 'HomeScreen'})
    _that.props.navigation.dispatch(resetAction);
  }
  PetDetailBtn=()=>{
    _that.validationAndApiParameter('Save')
  }



  render() {
    const headerProp = {
      title: 'Edit Pet',
      screens: '',
    };

    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>
        <ScrollView style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
          <View style={styles.centerbox}>
            <View style={styles.boxmain}>
              <View style={styles.equalbox}>
                <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)} style={{height:120,width:120}}>
                  <View style={[styles.avatar,  styles.avatarContainer,{ marginBottom: 20}]}>
                    {this.state.avatarSource != null ?
                      <Image style={styles.avatar1} source={this.state.avatarSource} />
                      :
                      this.state.data.image!=''?
                      <Image source={{uri: Constant.image_path2+this.state.data.image}} style={styles.avatar1}/>
                      :
                      <Image style={styles.avatar1} source={uploadimg} />
                    }
                  </View>
                </TouchableOpacity>
                <View style={styles.e_box1}>
                    <TextInput
                    style={styles.input}
                    ref='name'
                    placeholder = "Pet Name"
                    onChangeText={(name) => this.setState({name})}
                    value={this.state.name}
                    />
                </View>
              </View>
              <View style={styles.fullin}>
                <Picker
                  selectedValue={this.state.pet_type}
                  style={styles.input1}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({pet_type: itemValue})
                  }>
                  <Picker.Item label="Select pet type" value="" />
                  <Picker.Item label="Dog" value="Dog" />
                  <Picker.Item label="Cat" value="Cat" />
                  <Picker.Item label="Aquatic Animal" value="Aquatic Animal" />
                  <Picker.Item label="Bird" value="Bird" />
                  <Picker.Item label="Small Mammal" value="Small Mammal" />
                  <Picker.Item label="Large Mammal" value="Large Mammal" />
                  <Picker.Item label="Amphibian" value="Amphibian" />
                  <Picker.Item label="Other" value="Other" />
                </Picker>
              </View>
              <TextInput
                  style={styles.input}
                  ref='pet_breed'
                  placeholder = "Pet Breed"
                  onChangeText={(pet_breed) => this.setState({pet_breed})}
                  value={this.state.pet_breed}
              />
              <TextInput
                  style={styles.input}
                  placeholder = "Color"
                  ref='color'
                  onChangeText={(color) => this.setState({color})}
                  value={this.state.color}
              />
              <TextInput
                  style={styles.input}
                  placeholder = "Age"
                  ref='age'
                  onChangeText={(age) => this.setState({age})}
                  value={this.state.age}
              />
              <View style={styles.fullin}>
                <Picker
                  selectedValue={this.state.gender}
                  style={styles.input1}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({gender: itemValue})
                  }>
                    <Picker.Item label="Gender" value="" />
                    <Picker.Item label="Male" value="Male" />
                    <Picker.Item label="Female" value="Female" />
                </Picker>
              </View>

              <View style={styles.fullin}>
                <Picker
                  selectedValue={this.state.status}
                  style={styles.input1}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({status: itemValue})
                  }>
                    <Picker.Item label="Status" value="" />
                    <Picker.Item label="Neutered" value="Neutered" />
                    <Picker.Item label="Not neutered" value="Not neutered" />
                </Picker>
              </View>
              <TextInput
                  style={styles.input}
                  placeholder = "Weight"
                  ref='weight'
                  onChangeText={(weight) => this.setState({weight})}
                  value={this.state.weight}
              />
              <TextInput
                  style={styles.input}
                  ref='height'
                  placeholder = "Height"
                  onChangeText={(height) => this.setState({height})}
                  value={this.state.height}
              />
            </View>
          </View>
          <TouchableOpacity style={styles.btnblksign} onPress={this.PetDetailBtn} activeOpacity={.6} >
            <View style={{ width: '100%', justifyContent:'space-around',alignItems:'center'}}>
              <View style={{width: '50%',backgroundColor:'#c41a39', height:AppSizes.screen.width/7, borderRadius: 50, alignItems:'center', justifyContent: 'center',shadowColor: '#000',shadowOffset: { width: 2, height: 2 },shadowOpacity: 0.5,shadowRadius: 3,elevation: 2,zIndex:0,}}>
                  <Text style={styles.textbtn}>Update</Text>
              </View>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center',
    width:'100%',
    backgroundColor:'#fff'
  },
  content:{
    width:'100%',
  },
  centerbox:{
    width:'100%',
    alignItems:'center'
  },
  boxmain:{
    width:'90%',
    alignItems:'center',
    paddingTop:AppSizes.ResponsiveSize.Padding(2)
  },
  input:{
    width:'100%',
    height:60,
    paddingLeft:0,
    borderBottomWidth:1,
    borderBottomColor:'#000',
    color:'#000',
    marginBottom:15
  },
  input1:{
    width:'100%',
    height:60,
    paddingLeft:0,
    color:'#a3a3a3',
    borderBottomWidth:1,
    borderBottomColor:'#a3a3a3',
    color:'#000',
  },
  label:{
    width:'100%',
    marginBottom:AppSizes.ResponsiveSize.Padding(4),
    fontSize:AppSizes.ResponsiveSize.Sizes(15),
    fontWeight:'400',
    color:'#000',
  fontFamily:Fonts.Robotor
  },
  quebox:{
    width:'100%',
    marginBottom:20
  },
  radioinput:{
    //marginBottom:10
  //flexDirection:'row',
  //justifyContent:'space-between'
  },
  btnblksign:{
    width:'100%',
    alignItems:'center',
    marginTop:15
  },
  btnblksign1:{
    width:'100%',
    alignItems:'center',
    marginTop:0
  },
  productimg:{
    height:100,
    width:'80%',

  },
  inputdrop:{
    width:'100%',
    backgroundColor:'red'
  },
  fullin:{
    width:'100%',
    borderBottomWidth:1,
    borderBottomColor:'#000',
    marginBottom:15
    //backgroundColor:'red'
  },
  equalbox:{
    flexDirection:'row',
    justifyContent:'space-between'
  },
  e_box:{
    width:'30%',
  },
  e_box1:{
    width:'65%',
  },
  textbtn:{
    color:'#ffffff',
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
      fontFamily:Fonts.Robotor
  },
  textbtn1:{
    color:'#000',
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
      fontFamily:Fonts.Robotor
  },
  shadow:{
    justifyContent:'center',
  },
    avatar: {
     borderRadius: 75,
     width: 130,
     height: 130,
     marginBottom:15
    },
      avatar1: {
       borderRadius: 75,
       width: 100,
       height: 100,
       marginBottom:15
     },
     btnblksign:{
       marginBottom:5
     }
});
