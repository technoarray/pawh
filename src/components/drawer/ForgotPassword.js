import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput,Alert} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import CommonButton from '../common/CommonButton';
import SelectButton from '../common/SelectButton';
import * as commonFunctions from '../../utils/CommonFunctions';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import { Fonts } from '../../utils/Fonts';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

const background = require('../../themes/Images/background.png')
const logo = require('../../themes/Images/logo.png')
const facebook = require('../../themes/Images/facebook.png')
const google = require('../../themes/Images/googleplus.png')
const user = require('../../themes/Images/user.png')
const password = require('../../themes/Images/password.png')

let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      email:''
    },
    _that = this;
  }


    Signupbtn=()=>{
      const resetAction = NavigationActions.navigate({ routeName: 'SigninScreen'})
      _that.props.navigation.dispatch(resetAction);
    }
    handleOnPress=()=>{
      _that.validationAndApiParameter()
    }

    validationAndApiParameter() {
          const { email} = this.state
          var error=0

          if ((email.indexOf(' ') >= 0 || email.length <= 0)) {
            //this.setState({emailErrorMessage:'Please enter Email address!'});
            error=1;
            Alert.alert('Login Error','Please enter Email address!');
          }else if (!commonFunctions.validateEmail(email)) {
            error=1;
            Alert.alert('Login Error','Please enter valid Email address!');
          }
          if(error==0){
             var data = {
               email: email,
             };
            this.postToApiCalling('POST', 'login', Constant.URL_Forgot, data);
          }
    }

    postToApiCalling(method, apiKey, apiUrl, data) {

       new Promise(function(resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
                resolve(WebServices.callWebService_GET(apiUrl, data));
            }
        }).then((jsonRes) => {
            if ((!jsonRes) || (jsonRes.code == 0)) {
              console.log(jsonRes);
              Alert.alert("Error",jsonRes.message)
            }else {
              if (jsonRes.code == 1) {
                  _that.apiSuccessfullResponse(apiKey, jsonRes)
              }
            }
        }).catch((error) => {
            console.log("ERROR" + error);
            setTimeout(()=>{
                Alert.alert("Server issue" + error);
            },200);
        });
    }

    apiSuccessfullResponse(apiKey, jsonRes) {
      if (apiKey == 'login') {
        Alert.alert('Message','Verification mail has been sent to your email')
      }
    }

  render() {
    return (
      <View style={styles.container}>
          <ImageBackground source={background} style={{width: '100%', height: '100%'}}>
            <KeyboardAwareScrollView>
              <View style={styles.logosec}>

                    <Image source={logo} style={styles.logo}/>

                  <View style={styles.box}>

                  <View style={styles.inputbox}>
                      {/*<Image source={user} style={styles.iconinput}/>*/}
                        <TextInput style = {[styles.input,styles.shadow]}
                          ref='email'
                          placeholder = "Email"
                          placeholderTextColor = "#fff"
                          autoCapitalize = "none"
                          keyboardType={ 'email-address'}
                          onChangeText = {(email)=> this.setState({email})}/>
                  </View>

                    <TouchableOpacity style={styles.btnblksign} onPress={this.handleOnPress} activeOpacity={.6} >
                      <CommonButton label='Submit'/>
                      </TouchableOpacity>

                        </View>
<TouchableOpacity style={styles.btnblk} onPress={this.Signupbtn} activeOpacity={.6} >
    <View  style={{width:'100%',backgroundColor:'transparent', height:AppSizes.screen.width/7,alignItems:'center', justifyContent: 'center',paddingLeft:AppSizes.ResponsiveSize.Padding(1),paddingRight:AppSizes.ResponsiveSize.Padding(1),borderRadius:50}}>
        <Text style={styles.textbtn}>Back to LOGIN</Text>
    </View>
</TouchableOpacity>
              </View>



  </KeyboardAwareScrollView>
          </ImageBackground>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center'
  },
  logosec:{
      height:AppSizes.screen.height/1,
      justifyContent:'center',
     //backgroundColor:'red',
      alignItems:'center',
      position:'relative'
  },

  logo:{
    width:180,
    height:175,
    marginBottom:AppSizes.ResponsiveSize.Padding(5)
  },
  textbtn:{
    color:'#fff',
    fontSize:AppSizes.ResponsiveSize.Sizes(17),
  fontFamily:Fonts.Robotor
  },
  textbtnn:{
    color:'#fff',
    fontSize:AppSizes.ResponsiveSize.Sizes(16),
      fontFamily:Fonts.Robotor
  },
  input: {
    marginBottom:AppSizes.ResponsiveSize.Padding(3),
     height: 58,
     backgroundColor:'transparent',
     borderRadius:50,
     paddingLeft:25,
     width:'100%',
     borderColor:'#fff',
     borderWidth:2,
     padding:10,
     color:'#fff',
     fontSize:AppSizes.ResponsiveSize.Sizes(18),
       fontFamily:Fonts.Robotor
  },
  border:{
    height:2,
    backgroundColor:'#fff',
    width:60,
    marginBottom:20
  },
  title:{
    color:'#fff',
    fontSize:AppSizes.ResponsiveSize.Sizes(20),
    marginBottom:10,
      fontFamily:Fonts.Robotor
  },
  box:{
    width:'90%',
    padding:0,
    alignItems:'center',
    marginLeft:'auto',
    marginRight:'auto',
    borderRadius:20,
  },
  inputbox:{
    width:'100%',
  //  backgroundColor:'gray',
    position:'relative',
    marginBottom:AppSizes.ResponsiveSize.Padding(3)
  },

  btnblk:{
    width:'100%',
    position:'absolute',
    bottom:20,
    //backgroundColor:'red'
},
btnblksign:{
    width:'100%',
  marginTop:AppSizes.ResponsiveSize.Padding(2)
},
bottomblk:{
  justifyContent:'flex-end',
  height:AppSizes.screen.height/6,
  //backgroundColor:'green',
  flexDirection:'row',
  width:'90%',
  padding:0,
  alignItems:'center',
  marginLeft:'auto',
  marginRight:'auto',
  position:'absolute',
  bottom:10
},
icon:{
  height:50,
  width:50,
  marginLeft:AppSizes.ResponsiveSize.Padding(5)
},
iconinput:{
  height:50,
  width:50,
  position:'absolute',
  top:8,
  left:8
},
shadow:{

  justifyContent:'center',
  backgroundColor:'transparent',
  borderColor: '#fff',
  shadowColor: '#000',
  shadowOffset: { width: 2, height: 2 },
  shadowOpacity: 0.5,
  shadowRadius: 3,
  elevation: 2,
  zIndex:0,
  //backgroundColor:'red',
},
});
