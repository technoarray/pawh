import React, { Component } from 'react';
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput,Picker,Alert} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import CommonButton from '../common/CommonButton';
import SelectButton from '../common/SelectButton';
import Header from '../common/Headerwithback'
import { Icon } from 'react-native-elements'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import CheckBox from 'react-native-check-box'
import SelectInput from '@tele2/react-native-select-input';
import ImagePicker from 'react-native-image-picker';

var Constant = require('../../api/ApiRules').Constant;
var WebServices = require('../../api/ApiRules').WebServices;

import { Fonts } from '../../utils/Fonts';

import Swiper from 'react-native-swiper-animated';

const property = require('../../themes/Images/property.jpg')
const uploadimg = require('../../themes/Images/image-upload.png')


var radio_props = [
  {label: 'param1  ', value: 0 },
  {label: 'param2', value: 1 }
];
var radio_props1 = [
  {label: 'Within a week  ', value: 0 },
  {label: 'Within a month', value: 1 },
  {label: 'Not ready yet, just researching', value: 1 }
];

let _that
export default class HomeScreen extends Component {
  constructor (props) {
      super(props)
      this.state = {
      isVisible: false,
      avatarSource:null,
      name:'',
      pet_breed:'',
      color:'',
      age:'',
      weight:'',
      height:'',
      pet_type:'',
      gender:'',
      status:''
    },
    _that = this;
  }

  componentWillMount(){
    AsyncStorage.getItem('UserData').then((UserData) => {
      var data = JSON.parse(UserData);
        uid=data.id
        if(uid != ''){
          _that.setState({
            uid:uid,
          })
          _that.validationAndApiParameter('Show',uid)
        }
    })
  }
    selectPhotoTapped() {
      const options = {
        quality: 1.0,
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true,
        },
      };
        ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);

        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          const source = { uri: response.uri };

          this.setState({
            avatarSource: source,
          });
        }
      });
     }

     validationAndApiParameter(apiKey,uid) {
       if(apiKey=='Show'){
         var data={
           uid:uid
         }
         console.log(data);
         this.postToApiCalling('POSTs', 'Save', Constant.URL_showMyPet, data);
       }
       if(apiKey=='Save'){
         const { uid,name,pet_type,pet_breed,color,age,gender,status,weight,height,avatarSource } = this.state
         var error=0
         if(name==''){
           error=1
           Alert.alert('Input Error!','Please enter name')
         }
         if(gender=='Gender'){
           this.setState({gender:''})
         }

         if(error==0){
           // var myData=JSON.stringify([{var_name:'name',value:name},{var_name:'pet_type',value:pet_type},{var_name:'breed',value:pet_breed},{var_name:'color',value:color},{var_name:'age',value:age},{var_name:'gender',value:gender},{var_name:'status',value:status},{var_name:'weight',value:weight},{var_name:'height',value:height}])
           // var data = {
           //   uid:uid,
           //   variable:myData
           // };
           const data = new FormData();
           data.append('method','addMypet')
           data.append('uid', uid);
           data.append('name', name);
           data.append('pet_type', pet_type);
           data.append('breed', pet_breed);
           data.append('color', color);
           data.append('age', age);
           data.append('gender', gender);
           data.append('status', status);
           data.append('weight', weight);
           data.append('height', height);
           if(avatarSource!=null){data.append('image', {
             uri:  avatarSource.uri,
             type: 'image/jpeg', // or photo.type
             name: 'petImage.jpg'
           });}
           console.log(data);
           this.postToApiCalling('POST', 'Save', Constant.New_BASE, data);
         }
       }
     }

     postToApiCalling(method, apiKey, apiUrl, data) {
        new Promise(function(resolve, reject) {
             if (method == 'POST') {
                 resolve(WebServices.callWebServices(apiUrl, data));
             }
             else  if('POSTs'){
                 resolve(WebServices.callWebService(apiUrl, data));
             }
             else{
               resolve(WebServices.callWebService_GET(apiUrl, data));
             }
         }).then((responseJson) => {
           console.log(responseJson);
             if ((!responseJson) || (responseJson.code == 0)) {
               console.log(responseJson);
               Alert.alert("Error",responseJson.message)
               _that.setState({ isVisible: false })
             }else {
               if (responseJson.code == 1) {
                 _that.props.navigation.navigate('Pet_detail',{home:false,service:false,more:false,pet:true})
               }
               else if (responseJson.code==2) {
                 _that.apiSuccessfullResponse(apiKey, responseJson)
               }
             }
         }).catch((error) => {
             console.log("ERROR" + error);
             _that.setState({ isVisible: false })
             setTimeout(()=>{
                 Alert.alert("Server issue" + error);
             },200);
         });
     }

     apiSuccessfullResponse(apiKey, jsonRes) {
       console.log(jsonRes);
     }



  PetBtn=()=>{
    const resetAction = NavigationActions.navigate({ routeName: 'HomeScreen'})
    _that.props.navigation.dispatch(resetAction);
  }
  PetDetailBtn=()=>{
    _that.validationAndApiParameter('Save')
    // const resetAction = NavigationActions.navigate({ routeName: 'Pet_detail'})
    // _that.props.navigation.dispatch(resetAction);
  }



  render() {
    const headerProp = {
      title: 'My Pet',
      screens: '',
    };

    return (

      <View style={styles.container}>
        <Header info={headerProp} navigation={_that.props.navigation}/>
        <ScrollView style={[styles.content,this.state.Modal_Visibility ? {backgroundColor: 'rgba(0,0,0,0.4)',zIndex:5} : '#ffffff']}>
          <View style={styles.centerbox}>
            <View style={styles.boxmain}>
              <View style={styles.equalbox}>
                <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)} style={{height:120,width:120}}>
                  <View style={[styles.avatar,  styles.avatarContainer,{ marginBottom: 20}]}>
                    {this.state.avatarSource === null ? (
                      <Image source={uploadimg} style={styles.productimg}/>
                    ) : (
                      <Image style={styles.avatar1} source={this.state.avatarSource} />
                    )}
                  </View>
                </TouchableOpacity>
                <View style={styles.e_box1}>
                    <TextInput
                    style={styles.input}
                    ref='name'
                    placeholder = "Pet Name"
                    keyboardType={ 'default'}
                    onChangeText={(name) => this.setState({name})}
                    value={this.state.name}
                    />
                </View>
              </View>
              <View style={styles.fullin}>
                <Picker
                  selectedValue={this.state.pet_type}
                  style={styles.input1}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({pet_type: itemValue})
                  }>
                  <Picker.Item label="Select pet type" value="select" />
                  <Picker.Item label="Dog" value="Dog" />
                  <Picker.Item label="Cat" value="Cat" />
                  <Picker.Item label="Aquatic Animal" value="Aquatic Animal" />
                  <Picker.Item label="Bird" value="Bird" />
                  <Picker.Item label="Small Mammal" value="Small Mammal" />
                  <Picker.Item label="Large Mammal" value="Large Mammal" />
                  <Picker.Item label="Amphibian" value="Amphibian" />
                  <Picker.Item label="Other" value="Other" />
                </Picker>
              </View>
              <TextInput
                  style={styles.input}
                  ref='pet_breed'
                  keyboardType={ 'default'}
                  placeholder = "Pet Breed"
                  onChangeText={(pet_breed) => this.setState({pet_breed})}
                  value={this.state.pet_breed}
              />
              <TextInput
                  style={styles.input}
                  placeholder = "Color"
                  keyboardType={ 'default'}
                  ref='color'
                  onChangeText={(color) => this.setState({color})}
                  value={this.state.color}
              />
              <TextInput
                  style={styles.input}
                  placeholder = "Age"
                  keyboardType={ 'default'}
                  ref='age'
                  onChangeText={(age) => this.setState({age})}
                  value={this.state.age}
              />
              <View style={styles.fullin}>
                <Picker
                  selectedValue={this.state.gender}
                  style={styles.input1}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({gender: itemValue})
                  }>
                    <Picker.Item label="Gender" value="gender" />
                    <Picker.Item label="Male" value="Male" />
                    <Picker.Item label="Female" value="Female" />
                </Picker>
              </View>

              <View style={styles.fullin}>
                <Picker
                  selectedValue={this.state.status}
                  style={styles.input1}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({status: itemValue})
                  }>
                    <Picker.Item label="Status" value="status" />
                    <Picker.Item label="Neutered" value="Neutered" />
                    <Picker.Item label="Not neutered" value="Not neutered" />
                </Picker>
              </View>
              <TextInput
                  style={styles.input}
                  placeholder = "Weight"
                  ref='weight'
                  keyboardType={ 'default'}
                  onChangeText={(weight) => this.setState({weight})}
                  value={this.state.weight}
              />
              <TextInput
                  style={styles.input}
                  ref='height'
                  placeholder = "Height"
                  keyboardType={ 'default'}
                  onChangeText={(height) => this.setState({height})}
                  value={this.state.height}
              />
            </View>
          </View>
          <TouchableOpacity style={styles.btnblksign} onPress={this.PetDetailBtn} activeOpacity={.6} >
            <View style={{ width: '100%', justifyContent:'space-around',alignItems:'center'}}>
              <View style={{width: '50%',backgroundColor:'#c41a39', height:AppSizes.screen.width/7, borderRadius: 50, alignItems:'center', justifyContent: 'center',shadowColor: '#000',shadowOffset: { width: 2, height: 2 },shadowOpacity: 0.5,shadowRadius: 3,elevation: 2,zIndex:0,}}>
                  <Text style={styles.textbtn}>Continue</Text>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnblksign1}  activeOpacity={.6} onPress={this.PetBtn}>
            <View  style={{width:'100%', height:AppSizes.screen.width/10,alignItems:'center', justifyContent: 'center',paddingLeft:AppSizes.ResponsiveSize.Padding(1),paddingRight:AppSizes.ResponsiveSize.Padding(1)}}>
                <Text style={styles.textbtn1}>Skip</Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center',
    width:'100%',
    backgroundColor:'#fff'
  },
  content:{
    width:'100%',
  },
  centerbox:{
    width:'100%',
    alignItems:'center'
  },
  boxmain:{
    width:'90%',
    alignItems:'center',
    paddingTop:AppSizes.ResponsiveSize.Padding(2)
  },
  input:{
    width:'100%',
    height:60,
    paddingLeft:0,
    borderBottomWidth:1,
    borderBottomColor:'#000',
    color:'#000',
    marginBottom:15,
    fontFamily:Fonts.Robotor
  },
  input1:{
    width:'100%',
    height:60,
    paddingLeft:0,
    color:'#a3a3a3',
    borderBottomWidth:1,
    borderBottomColor:'#a3a3a3',
    color:'#000',
    fontFamily:Fonts.Robotor
  },
  label:{
    width:'100%',
    marginBottom:AppSizes.ResponsiveSize.Padding(4),
    fontSize:AppSizes.ResponsiveSize.Sizes(15),
    fontWeight:'400',
    color:'#000',
fontFamily:Fonts.Robotor
  },
  quebox:{
    width:'100%',
    marginBottom:20
  },
  radioinput:{
    //marginBottom:10
  //flexDirection:'row',
  //justifyContent:'space-between'
  },
  btnblksign:{
    width:'100%',
    alignItems:'center',
    marginTop:15
  },
  btnblksign1:{
    width:'100%',
    alignItems:'center',
    marginTop:0
  },
  productimg:{
    height:100,
    width:'80%',

  },
  inputdrop:{
    width:'100%',
    backgroundColor:'red'
  },
  fullin:{
    width:'100%',
    borderBottomWidth:1,
    borderBottomColor:'#000',
    marginBottom:15
    //backgroundColor:'red'
  },
  equalbox:{
    flexDirection:'row',
    justifyContent:'space-between'
  },
  e_box:{
    width:'30%',
  },
  e_box1:{
    width:'65%',
  },
  textbtn:{
    color:'#ffffff',
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    fontFamily:Fonts.Robotor
  },
  textbtn1:{
    color:'#000',
    fontFamily:Fonts.Robotor,
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
  },
  shadow:{
    justifyContent:'center',
  }
  ,
    avatar: {
     borderRadius: 75,
     width: 130,
     height: 130,
     marginBottom:15
    },
      avatar1: {
       borderRadius: 75,
       width: 100,
       height: 100,
       marginBottom:15
      }
});
