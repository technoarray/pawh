import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,Button} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'

import { Fonts } from '../../utils/Fonts';

export default class SelectButton extends Component {
  render() {
    return (

      <View  style={{width:'100%', height:AppSizes.screen.width/10,alignItems:'center', justifyContent: 'center',paddingLeft:AppSizes.ResponsiveSize.Padding(1),paddingRight:AppSizes.ResponsiveSize.Padding(1)}}>
          <Text style={styles.textbtn}>{this.props.label}</Text>
      </View>

    );
  }
}

const styles = StyleSheet.create({
textbtn:{
  color:'#fff',
  fontSize:AppSizes.ResponsiveSize.Sizes(17),
  fontFamily:Fonts.Robotor
}
});
