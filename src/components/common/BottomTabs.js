import React,{Component} from 'react'
import {AsyncStorage,StyleSheet,Text,View,Image,TouchableOpacity,Platform,ImageBackground,ScrollView,TextInput,Picker} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'

import { Fonts } from '../../utils/Fonts';

var home=require('../../themes/Images/2.png')
var pet=require('../../themes/Images/1.png')
var service=require('../../themes/Images/3.png')
var more=require('../../themes/Images/4.png')

export default class App extends Component {

    Addbtn=()=>{
      _that.props.navigation.navigate('Add_adoption');
    }
    Homebtn=()=>{
      _that.props.navigation.navigate('HomeScreen');
    }
    Servicebtn=()=>{
      _that.props.navigation.navigate('ServicesScreen');
    }

    Morebtn=()=>{
      _that.props.navigation.navigate('SettingScreen');
    }
    Petbtn=()=>{
      _that.props.navigation.navigate('Pet_detail_form');
    }

  render() {
    return (
      <View style={{backgroundColor:'#c41a39',zIndex:9999,width:'100%',height:50,flexDirection:'row',justifyContent:'space-around',position:'absolute',bottom:0}}>
        <TouchableOpacity onPress={this.Homebtn}  style={{height:'100%',justifyContent:'center',alignItems:'center'}}>
          <Image source={home} style={styles.icon}/>
          <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>Adopt</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.Petbtn} style={{height:'100%',justifyContent:'center',alignItems:'center'}}>
          <Image source={pet} style={styles.icon}/>
          <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>My Pet</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.Servicebtn} style={{height:'100%',justifyContent:'center',alignItems:'center'}}>
          <Image source={service} style={styles.icon}/>
          <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>Services</Text>
        </TouchableOpacity>
        <TouchableOpacity  onPress={this.Morebtn} style={{height:'100%',justifyContent:'center',alignItems:'center'}}>
          <Image source={more} style={styles.icon}/>
          <Text style={{color:'#fff',fontSize:AppSizes.ResponsiveSize.Sizes(9),fontFamily:Fonts.Robotor}}>More</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:  0,
    alignItems:'center',
    justifyContent:'center',
    width:'100%',
    backgroundColor:'#fff'
  },
  content:{
    width:'100%',

  },
  centerbox:{
    width:'100%',
    alignItems:'center'
  },
  boxmain:{
    width:'90%',
    alignItems:'center',
    paddingTop:AppSizes.ResponsiveSize.Padding(4),
    //backgroundColor:'red'
  },
  input:{
    width:'100%',
    height:40,
    paddingLeft:0,
  borderBottomWidth:1,
  borderBottomColor:'#a0a0a0',
  marginBottom:15
},
label:{
  width:'100%',
  marginBottom:AppSizes.ResponsiveSize.Padding(2),
  fontSize:AppSizes.ResponsiveSize.Sizes(13),
  fontWeight:'600',
  color:'#000',
  fontFamily:Fonts.Robotor
},
quebox:{
  width:'100%',
  marginBottom:10
},

btnblksign:{
  width:'100%'
},
full_btn:{
  width:'100%'
},
icon:{
  width:28,
  height:28
}

});
