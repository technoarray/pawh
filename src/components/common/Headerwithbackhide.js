import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,StatusBar,ScrollView} from 'react-native';
import { DrawerActions } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { AppStyles, AppSizes, AppColors } from '../../themes/'
import { NavigationActions, StackActions  } from 'react-navigation';
import { Icon } from 'react-native-elements'
/* Images */
var back = require( '../../themes/Images/back.png')
var user = require( '../../themes/Images/usermenu.png')


import { Fonts } from '../../utils/Fonts';

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);
export default class Header extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
    routename: '',
  },
  _that = this;
  }

  favbtn=()=>{
    const resetAction = NavigationActions.navigate({ routeName: 'Favlist'})
    _that.props.navigation.dispatch(resetAction);
  }
  Backbtn=()=>{
    _that.props.navigation.goBack(null);

  }

  menu_click() {
    //console.log(_that.props.navigation);
    //_that.props.navigation.openDrawer();
  }


    render() {

      const headerProp = {
        title: 'WELCOME',
        screens: '',
      };

      return (
      <LinearGradient colors={['#f4f4f4','#f4f4f4','#f4f4f4','#f4f4f4']} style={[styles.mainbar,styles.shadow]}>

        <MyStatusBar barStyle="light-content"  backgroundColor="#000"/>
          <View style={styles.appBar} >
              <View style={styles.header}>
                <View style={styles.centerbox}>
                    <View style={styles.imageContainer}>
                      <TouchableOpacity onPress={this.Backbtn} style={styles.menuWrapper}>
                            <Image style={styles.image} source={back}/>
                      </TouchableOpacity>
                    </View>

                    <View style={{ marginTop:0,width:'80%',alignItems:'center' }}>
                        <Text style={styles.headertitle}>ADOPT</Text>
                    </View>

                    <View style={styles.imageContainer}>
                      <TouchableOpacity style={styles.menuWrapper}>

                      </TouchableOpacity>
                    </View>
                </View>
              </View>
          </View>



      </LinearGradient>
    )
    }
}

const styles = StyleSheet.create({
  statusBar: {
    height: AppSizes.statusBarHeight,
  },
  appBar: {
    width:'100%',
      height: AppSizes.navbarHeight,
      justifyContent:'center',
      ...Platform.select({
      android: {
      paddingTop:AppSizes.ResponsiveSize.Padding(3),
      },
    }),
    },

    header:{
          flex:1,
          alignItems: 'center',
           width:'100%',
           paddingTop:0,

          },

      headertitle:{
        color:'#ffffff',
        fontSize:AppSizes.ResponsiveSize.Sizes(16),
        fontWeight:'bold',
        letterSpacing:1,
        fontFamily:Fonts.Robotor
      },
      imageContainer:{
        width:'10%',
      //  backgroundColor:'red'
      },
  logoimageContainer:{
    width:'50%'
  },
  logoWrapper:{
    width:'100%',
    height:'100%',
  },
  menuWrapper: {
    width:'100%',
    height:'100%',

  },
  image: {
    height:25,
    width:25
  },

  headertitle:{
    fontSize:AppSizes.ResponsiveSize.Sizes(18),
    color:'#000',
    fontWeight:'400',
    fontFamily:Fonts.Robotor
  },
  centerbox:{
    width:'90%',
    flexDirection:'row',
  //  backgroundColor:'red',
    justifyContent:'center'
  },
  shadow:{
    borderWidth:0,
    borderColor: '#fff',
    justifyContent:'center',
    backgroundColor:'#fff',
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#999',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    elevation: 1,
    zIndex:0,
    //backgroundColor:'red',
  },
  mainbar:{
    width:'100%'
  }


});
