import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,Button} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { AppStyles, AppSizes, AppColors } from '../../themes/'

import { Fonts } from '../../utils/Fonts';

export default class CommonButton extends Component {
  render() {
    return (
      <View style={{ backgroundColor:'red',alignItems:'center', justifyContent: 'center',justifyContent:'flex-end'}}>
        <View style={{ width: '100%'}}>
              <LinearGradient colors={['#c41a39','#c41a39','#c41a39','#c41a39']} style={{width: '100%', height:AppSizes.screen.width/8,  alignItems:'center', justifyContent: 'center'}}>
                  <Text style={styles.textbtn}>{this.props.label}</Text>
              </LinearGradient>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
textbtn:{
  color:'#ffffff',
  fontSize:AppSizes.ResponsiveSize.Sizes(15),
  fontWeight:'400',
  fontFamily:Fonts.Robotor

}
});
