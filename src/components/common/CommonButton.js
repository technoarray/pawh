import React, { Component } from 'react';
import {StyleSheet,Text,View,Image,TouchableOpacity,Platform,Alert,Button} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../../themes/'


import { Fonts } from '../../utils/Fonts';

export default class CommonButton extends Component {
  render() {
    return (

        <View style={{ width: '100%', justifyContent:'space-around'}}>
              <View style={{width: '100%',backgroundColor:'#c41a39', height:AppSizes.screen.width/7, borderRadius: 50, alignItems:'center', justifyContent: 'center',shadowColor: '#000',
              shadowOffset: { width: 2, height: 2 },
              shadowOpacity: 0.5,
              shadowRadius: 3,
              elevation: 2,
              zIndex:0,}}>
                  <Text style={styles.textbtn}>{this.props.label}</Text>
              </View>
        </View>

    );
  }
}

const styles = StyleSheet.create({
textbtn:{
  color:'#ffffff',
  fontSize:AppSizes.ResponsiveSize.Sizes(18),
  fontFamily:Fonts.Robotor
},
shadow:{

  justifyContent:'center',

  //backgroundColor:'red',
},
});
