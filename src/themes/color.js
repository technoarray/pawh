const app = {
    primary : '#3ca5b5',
    secondary : '#0c0c0c',
    background: '#FFFFFF',
    cardBackground: '#FFFFFF',
    listItemBackground: '#FFFFFF',
    contentColor:'#1f1f1f',
    subtitleColor:'#3e3e3e',
};

const brand = {
    brand: {
        primary: '#FFFFFF',
        secondary: '#17233D',
    },
};
const text = {
    textPrimary: 'black',
    textSecondary: '#777777',
    headingPrimary: brand.brand.primary,
    headingSecondary: brand.brand.primary,
};

const borders = {
    border: '#D0D1D5',
};

const tabbar = {
    tabbar: {
        background: '#ffffff',
        iconDefault: '#BABDC2',
        iconSelected: brand.brand.primary,
    },
};

const tintColor = '#2f95dc';


export default {
    ...app,
    ...brand,
    ...text,
    ...borders,
    ...tabbar,

};
