import React from 'react';
import {
    AppRegistry,
    StyleSheet,
    Dimensions,
    Platform
} from 'react-native';

const { width, height } = Dimensions.get("window");

import Colors from './color';
import Fonts from './fonts';
import Sizes from './sizes';
const borders = {
    border: '#D0D1D5',
};
const color = {
    green: '#35AA4E',
    white: '#fff',
    lightGray: '#FAF6F2',
    lightSilver: '#f9f8f7',
    purple: '#141764'
};
const Align = {
    textAlignCenter: 'center',
    alignSelfcenter: 'center',
}
const text = {
    fontWeightbold: 'bold',
    fontSize18: 18,
    fontSize12: 12
}
const flexDirection = {
    flexDirectioncolumn: 'column',
    flexDirectionrow: 'row',
    justifyContentcenter: 'center',
    alignItemsflexstart: 'flex-start',
    justifyContentspacebetween: 'space-between',
    alignItemscenter: 'center',
}
const Flex = {
    flex1: 1,
    flex2: 2,
    flex3: 3,
    flex4: 4,
    flex5: 5,
    flex6: 6,
    flex7: 7,
    flex8: 8,
    flex9: 9,
    flex10: 10
}
const Margin = {
    marginTop2: 2,
    marginTop4: 4,
    marginTop6: 6,
    marginTop8: 8,
    marginTop10: 10,
    marginTop20: 20,
}
export default {
    appContainer: {
        backgroundColor: '#FFF',
    },

    // Default
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: Colors.background,
    },
    container_txt_left: {
        textAlign: 'left',
        paddingLeft: 15,
        paddingTop: 8,
    },
    containerRight: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'flex-end',
        paddingRight: 5,
        paddingTop: 5
    },
    dash_touch: {
        height: 70,
        flexWrap: 'wrap',
    },
    containerCenter: {
        flex: 1,
        flexDirection: 'row',
        alignSelf: 'center',
    },
    contactus_hdrtxt: {
        flex: 1,
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    login_cnt: {
        justifyContent: 'center',
        alignItems: 'flex-end',
        flexDirection: 'row'
    },
    login_forgetpass: {
        alignItems: 'flex-end',
    },
    copyright: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: height / 14
    },
    copyright_txt: {
        color: '#00002f',
        fontSize: 10,
        textAlign: 'center',
        marginTop: 15,
        backgroundColor: 'rgba(0,0,0,0)',
    },
    InputCnt: {
        flexDirection: 'row',
        borderColor: '#141764',
        borderWidth: 1,
        borderRadius: 5,
        height: 50,
        marginTop: 10
    },
    InputCnthdrtxt: {
        height: 50,
        marginTop: 10,
        marginBottom: 10,
        color: '#141764',
        fontSize: 14,
    },
    contactus_cnt_submitbtn: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        width: width - 85,
        height: 50,
    },
    InputCnt_txt: {
        width: Sizes.screen.width - 50,
        margin: 5,
        marginLeft: 2,
        height: 50,
        marginTop: 4,
        fontSize: 15,
        color: '#005faf',
        borderBottomColor: '#005faf',
        borderBottomWidth: 2
    },
    Inputtxt: {
        flex: 6,
        height: 50,
        marginBottom: 10
    },
    Inputtxt_NonBorder: {
        flex: 6,
        height: 70,
        marginBottom: 5,
        marginTop: 3
    },
    InputCnt_txt_NonBorder: {
        width: Sizes.screen.width - 50,
        height: 35,
        marginTop: 4,
        fontSize: 15,
        color: '#005faf',
        borderWidth: 1,
        borderColor: "#005faf",
        paddingTop: 5,
        borderRadius: 5,
    },
    InputCnt_txt_login: {
        width: Sizes.screen.width - 100,
        margin: 5,
        marginLeft: 2,
        height: 40,
        marginTop: 4,
        fontSize: 15,
        color: '#005faf',
        borderBottomColor: '#005faf',
        borderBottomWidth: 2
    },

    registerInput: {
        width: width - 80,
        margin: 5,
        marginLeft: 10,
        height: 40,
        marginTop: 4,
        fontSize: 15,
    },
    Input_img: {
        width: 25,
        height: 20,
        marginLeft: 18,
        marginTop: 15,
    },
    InputCnt_icon: {
        width: 55,
        height: 50,
    },
    Input_iconcnt: {
        flexDirection: 'row',
        borderColor: '#FFF',
        borderWidth: 1.5,
        borderRadius: 3,
        height: 50,
        marginTop: 10,
        width: Sizes.screen.width,
    },
    InputCntmessage: {
        width: 250,
        textAlignVertical: 'top',
        margin: 5,
        marginLeft: 30,
        marginTop: 4,
        fontSize: 15,
        height: 120
    },

    GreenButton: {
        width: 170,
        height: 32,
        right: 5,
        borderRadius: 5,
        backgroundColor: '#35aa4e',
        position: 'absolute',
        bottom: 10,
        paddingTop: 6,
    },
    containerCentered: {
        paddingRight: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    windowSize: {
        height: Sizes.screen.height,
        width: Sizes.screen.width,
    },
    login_background: {
        width: width,
        height: height
    },
    forgot_txt: {
        color: '#005faf',
        fontSize: 15,
        textDecorationLine: 'underline',
        marginTop: 20,
        backgroundColor: 'rgba(0,0,0,0)',
    },
    // Aligning items
    leftAligned: {
        alignItems: 'flex-start',
    },
    centerAligned: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    rightAligned: {
        alignItems: 'flex-end',
    },
    aboutusLogo: {
        width: Sizes.screen.width - 100,
        resizeMode: 'center',
        height: Sizes.screen.height / 8,
        marginTop: 10,
    },
    linkUrl: {
        color: '#25a440'
    },

    touchableContainer: {
        height: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    touchableButton: {
        width: Sizes.screen.width - 90,
        height: 40,
        borderRadius: 5,
        backgroundColor: '#35AA4E',
        paddingTop: 10,
    },
    touchableText: {
        textAlign: 'center',
        color: '#fff',
        fontWeight: 'bold'
    },
    calender_icon: {
        width: 20,
        height: 20,
        paddingRight: 5,
        justifyContent: 'flex-end'
    },
    //************ */


    tab_container: {
        flex: 2,
        backgroundColor: color.lightGray,
        flexDirection: 'column',
        marginTop: (Platform.OS === 'ios') ? 0 : 0,
        backgroundColor: Colors.background,
    },
    Dash_container: {
        flex: .5,
        flexDirection: flexDirection.flexDirectionrow,
        alignItems: flexDirection.alignItemsflexstart,
    },
    Dash_Main_Card: {
        flex: 1,
        height: 90,
        flexWrap: 'wrap',
        padding: 10,
        elevation: 3,
        borderWidth: 1,
        borderColor: color.lightSilver,
        backgroundColor: color.white,
    },
    Dash_row: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 8,
        marginRight: 8,
    },
    dash_Mcnt: {
        backgroundColor: color.lightGray,
        height: height
    },
    AppBackground: {
        backgroundColor: '#fff',
    },
    Dash_MainVW: {
        flex: Flex.flex1,
        flexDirection: flexDirection.flexDirectioncolumn,
        justifyContent: flexDirection.justifyContentspacebetween,
        alignItems: flexDirection.alignItemscenter,
        marginTop: Margin.marginTop20,
        backgroundColor: color.lightGray
    },
    Dash_StartInspectionBtn: {
        alignItems: 'center',
        width: width - 100,
        height: 40,
        borderRadius: 5,
        backgroundColor: color.green,
        paddingTop: 10,
    },
    Dash_StartInspectionBtn_Text: {
        textAlign: Align.textAlignCenter,
        color: color.white
    },
    Dash_StartInspectionBtnVW: {
        height: 50,
        flexDirection: flexDirection.flexDirectioncolumn,
        justifyContent: flexDirection.justifyContentcenter,
        alignItems: flexDirection.alignItemscenter,
        backgroundColor: color.lightGray
    },
    Dash_Card: {
        padding: 20,
        flex: Flex.flex1,
        flexDirection: flexDirection.flexDirectionrow,
    },
    Dash_TxtBold: {
        fontWeight: text.fontWeightbold,
        fontSize: text.fontSize18
    },
    Dashcontenttxt: {
        width: 100,
        marginLeft: 10,
        fontSize: text.fontSize12,
        color: color.purple
    },
    Dash_icon: {
        width: 28,
        height: 30,
        marginTop: 5,
    },
    Dash_iconheader: {
        width: 26,
        height: 26
    },
    profile_img: {
        flex: Flex.flex1
    },
    top_backtxt: {
        marginTop: -25,
        color: color.white,
        textAlign: Align.textAlignCenter,
        alignSelf: Align.alignSelfcenter,
        fontWeight: 'bold',
        fontSize: 14,
    },
    MainContainer: {
        justifyContent: flexDirection.alignItemscenter,
        flex: Flex.flex1,
        margin: 10
    },
    top_backicon: {
        width: 24,
        height: 24,
        marginLeft: 10,
        marginTop: 15
    },
    icon: {
        width: 26,
        height: 26,
    },
    backgroundContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        height: 180
    },
    background: {
        width: null,
        height: null
    },
    forgot_submitButton: {
        width: width - 85,
        height: 50,
    },
    forgot_logo: {
        width: width - 180,
        height: 150,
        marginTop: 15,
        marginBottom: 10,
        resizeMode: 'cover',
    },
    logo: {
        borderRadius: 70,
        marginTop: 20,
        width: 135,
        height: 135,
        alignItems: 'center',
        alignSelf: 'center',
        borderWidth: 4,
        borderColor: '#ffffff',
    },
    backdrop: {
        width: width,
        height: 180,
        flexDirection: 'column'
    },
    headline: {
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        fontWeight: 'bold',
        backgroundColor: 'rgba(0,0,0,0)',
    },

    detailBox: {
        flexDirection: 'row',
        marginTop: 1,
        height: 130,
    },

    greenbottombtn: {
        bottom: 0,
        width: 200,
        height: 40,
        borderRadius: 50,
        backgroundColor: '#00943b',
        marginLeft: 50,
        marginTop: 0,
        paddingTop: 10,
        paddingBottom: 20,
    },
    logout_txt: {
        textAlign: 'center',
        color: '#fff',
        height: 30,
        marginLeft: 10,
        fontWeight: 'bold'
    },
    logintxt: {
        textAlign: 'center',
        color: '#fff',
        fontWeight: 'bold'
    },

    contactus_cnt: {
        flex: 1,
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 30,
        marginTop: 100,
        flexDirection: 'column'
    },

    plothistory_cnt: {
        height: 80,
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 20,
    },
    Plot_headertxt: {
        fontWeight: 'bold',
        marginTop: 10,
        fontSize: 18,
        color: '#000'
    },
    plot_contenttext: {
        color: '#000',
    },
    plot_content: {
        // backgroundColor: '#696ea4',
        width: width - 30,
        marginLeft: 15,
        paddingTop: 10,
        paddingLeft: 10,
        paddingBottom: 10,
        borderRightColor: '#696ea4',
        borderRightWidth: 1,
        borderLeftColor: '#696ea4',
        borderLeftWidth: 1,
        borderBottomColor: '#696ea4',
        borderBottomWidth: 1,

        //borderColor:'#696ea4'
    },
    plot_header: {
        // backgroundColor: '#f39c30',
        width: width - 30,
        marginLeft: 15,
        marginTop: 10,
        padding: 10,
        height: 40,
        borderRadius: 3,

    },
    plot_headerText: {
        color: '#fff',
        fontSize: 15
    },
    plot_reviewtouch: {
        width: width - 200,
        height: 40,
        borderRadius: 3,
        backgroundColor: '#35AA4E',
        paddingTop: 10,
    },
    dropdown_2_text: {
        marginVertical: 10,
        marginHorizontal: 6,
        fontSize: 16,
        color: '#141764',
        textAlign: 'left',
        marginLeft: 1,
        textAlignVertical: 'center',
    },
    dropdown_2_dropdown: {
        width: 150,
        height: 100,
        borderWidth: 2,
        borderColor: '#141764'
    },
    dropdown_3_dropdown: {
        width: 150,
        height: 100,
        borderWidth: 2,
        borderColor: '#141764'
    },
    dropdown_4_dropdown: {
        width: 240,
        height: 100,
        borderWidth: 2,
        borderColor: '#141764'
    },
    dropdown_2: {
        alignSelf: 'flex-start',
        width: 150,
        marginTop: 5,
        backgroundColor: '#00000000',
        left: 0,
        borderWidth: 0,
    },

    plot_arrowicon: {
        width: 18,
        height: 18
    },
    plothistorycontent: {
        width: 150,
        height: 50,
        borderRadius: 5,
        borderColor: '#141764',
        borderWidth: 1,
        right: 15
    },
    /************ */





    //-----------------------------------------------------------------------------

    // Text Styles
    baseText: {
        fontFamily: Fonts.base.family,
        fontSize: Fonts.base.size,
        lineHeight: Fonts.base.lineHeight,
        color: Colors.textPrimary,
    },
    p: {
        fontFamily: Fonts.base.family,
        fontSize: Fonts.base.size,
        lineHeight: Fonts.base.lineHeight,
        color: Colors.textPrimary,
        fontWeight: '500',
        marginBottom: 8,
    },
    h1: {
        fontFamily: Fonts.h1.family,
        fontSize: Fonts.h1.size,
        lineHeight: Fonts.h1.lineHeight,
        color: Colors.headingPrimary,
        fontWeight: '800',
        margin: 0,
        marginBottom: 4,
        left: 0,
        right: 0,
    },
    h2: {
        fontFamily: Fonts.h2.family,
        fontSize: Fonts.h2.size,
        lineHeight: Fonts.h2.lineHeight,
        color: Colors.headingPrimary,
        fontWeight: '800',
        margin: 0,
        marginBottom: 4,
        left: 0,
        right: 0,
    },
    h3: {
        fontFamily: Fonts.h3.family,
        fontSize: Fonts.h3.size,
        lineHeight: Fonts.h3.lineHeight,
        color: Colors.headingPrimary,
        fontWeight: '500',
        margin: 0,
        marginBottom: 4,
        left: 0,
        right: 0,
    },
    h4: {
        fontFamily: Fonts.h4.family,
        fontSize: Fonts.h4.size,
        lineHeight: Fonts.h4.lineHeight,
        color: Colors.headingPrimary,
        fontWeight: '800',
        margin: 0,
        marginBottom: 4,
        left: 0,
        right: 0,
    },
    h5: {
        fontFamily: Fonts.h5.family,
        fontSize: Fonts.h5.size,
        lineHeight: Fonts.h5.lineHeight,
        color: Colors.headingPrimary,
        fontWeight: '800',
        margin: 0,
        marginTop: 4,
        marginBottom: 4,
        left: 0,
        right: 0,
    },
    strong: {
        fontWeight: '900',
    },
    link: {
        textDecorationLine: 'underline',
        color: Colors.brand.primary,
    },
    subtext: {
        fontFamily: Fonts.base.family,
        fontSize: Fonts.base.size * 0.8,
        lineHeight: parseInt(Fonts.base.lineHeight * 0.8, 10),
        color: Colors.textSecondary,
        fontWeight: '500',
    },

    // Helper Text Styles
    textCenterAligned: {
        textAlign: 'center',
    },
    textRightAligned: {
        textAlign: 'right',
    },

    // Give me padding
    padding: {
        paddingVertical: Sizes.padding,
        paddingHorizontal: Sizes.padding,
    },
    paddingHorizontal: {
        paddingHorizontal: Sizes.padding,
    },
    paddingLeft: {
        paddingLeft: Sizes.padding,
    },
    paddingRight: {
        paddingRight: Sizes.padding,
    },
    paddingVertical: {
        paddingVertical: Sizes.padding,
    },
    paddingTop: {
        paddingTop: Sizes.padding,
    },
    paddingBottom: {
        paddingBottom: Sizes.padding,
    },
    paddingSml: {
        paddingVertical: Sizes.paddingSml,
        paddingHorizontal: Sizes.paddingSml,
    },
    paddingHorizontalSml: {
        paddingHorizontal: Sizes.paddingSml,
    },
    paddingLeftSml: {
        paddingLeft: Sizes.paddingSml,
    },
    paddingRightSml: {
        paddingRight: Sizes.paddingSml,
    },
    paddingVerticalSml: {
        paddingVertical: Sizes.paddingSml,
    },
    paddingTopSml: {
        paddingTop: Sizes.paddingSml,
    },
    paddingBottomSml: {
        paddingBottom: Sizes.paddingSml,
    },

    // General HTML-like Elements
    hr: {
        left: 0,
        right: 0,
        borderBottomWidth: 1,
        borderBottomColor: Colors.border,
        height: 1,
        backgroundColor: 'transparent',
        marginTop: Sizes.padding,
        marginBottom: Sizes.padding,
    },

    // Grid
    row: {
        left: 0,
        right: 0,
        flexDirection: 'row',
    },
    flex1: {
        flex: 1,
    },
    flex2: {
        flex: 2,
    },
    flex3: {
        flex: 3,
    },
    flex4: {
        flex: 4,
    },
    flex5: {
        flex: 5,
    },
    flex6: {
        flex: 6,
    },

    // Navbar
    navbar: {
        backgroundColor: Colors.brand.primary,
        borderBottomWidth: 0,
    },
    navbarTitle: {
        color: '#ffffff',
        fontWeight: 'bold',
        fontFamily: Fonts.base.family,
        fontSize: Fonts.base.size,
    },
    navbarButton: {
        tintColor: '#ffffff',
    },

    // TabBar
    tabbar: {
        backgroundColor: Colors.tabbar.background,
        borderTopColor: Colors.border,
        borderTopWidth: 1,
    },
	
	// Advertise banner
	
	advertise_text:{
		color:'#ffffff',
	},
	advertise_container:{
		height: 20, 
		flexDirection: 'row',
		alignItems:'center',
		justifyContent: 'center',
		backgroundColor: '#0c71a5',
		marginBottom:10
	},
	
};