import React, {Component} from 'react';
import { Platform, StyleSheet, TouchableOpacity, Image,Text, View } from 'react-native';
import SplashScreen from 'react-native-smart-splash-screen'

import App from './app';

export default class Index extends Component{
  componentWillMount() {
      if (Platform.OS === 'android') {
          SplashScreen.close({
              animationType: SplashScreen.animationType.scale,
              duration: 850,
              delay: 2000
          })
      }
  }

  render() {
    return (
      <App/>
    );
  }

}
