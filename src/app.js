import React, {Component} from 'react';
import { StackNavigator } from "react-navigation";

import SigninScreen from './components/drawer/SigninScreen';
import ForgotPassword from './components/drawer/ForgotPassword';
import SignUpScreen from './components/drawer/SignUpScreen';

import HomeScreen from './components/drawer/HomeScreen';
import SettingScreen from './components/drawer/SettingScreen';
import SingleScreen from './components/drawer/SingleScreen';
import Request_adoption from './components/drawer/Request_adoption';
import Add_adoption from './components/drawer/Add_adoption';
import Pet_detail from './components/drawer/Pet_detail';
import edit_profile from './components/drawer/edit_profile';
import Report_stray from './components/drawer/Report_stray';
import Favlist from './components/drawer/Favlist';
import Pet_detail_form from './components/drawer/Pet_detail_form';
import ServicesScreen from './components/drawer/ServicesScreen';
import locationScreen from './components/drawer/locationScreen';

import AnimalFriendly from './components/drawer/AnimalFriendly';
import Petgroomers from './components/drawer/Petgroomers';
import PetShelters from './components/drawer/PetShelters';
import PetTraining from './components/drawer/PetTraining';
import Edit_pet_detail from './components/drawer/Edit_pet_detail'
import GiveFeedback from './components/drawer/GiveFeedback'

const AppNavigator = StackNavigator({

  SigninScreen: {
      screen: SigninScreen,
      navigationOptions: {
          header: null
      }
  },
  SignUpScreen: {
      screen: SignUpScreen,
      navigationOptions: {
          header: null
      }
  },

  ForgotPassword: {
      screen: ForgotPassword,
      navigationOptions: {
          header: null
      }
  },

  HomeScreen: {
      screen: HomeScreen,
      navigationOptions: {
          header: null
      }
  },

  SettingScreen: {
      screen: SettingScreen,
      navigationOptions: {
          header: null
      }
  },
  SingleScreen: {
      screen: SingleScreen,
      navigationOptions: {
          header: null
      }
  },
  Request_adoption: {
      screen: Request_adoption,
      navigationOptions: {
          header: null
      }
  },
  Add_adoption: {
      screen: Add_adoption,
      navigationOptions: {
          header: null
      }
  },
  Pet_detail: {
      screen: Pet_detail,
      navigationOptions: {
          header: null
      }
  },
  edit_profile: {
      screen: edit_profile,
      navigationOptions: {
          header: null
      }
  },

  Report_stray: {
      screen: Report_stray,
      navigationOptions: {
          header: null
      }
  },

  Favlist: {
      screen: Favlist,
      navigationOptions: {
          header: null
      }
  },

  Pet_detail_form: {
      screen: Pet_detail_form,
      navigationOptions: {
          header: null
      }
  },
  ServicesScreen: {
      screen: ServicesScreen,
      navigationOptions: {
          header: null
      }
  },
  locationScreen: {
      screen: locationScreen,
      navigationOptions: {
          header: null
      }
  },
  AnimalFriendly: {
      screen: AnimalFriendly,
      navigationOptions: {
          header: null
      }
  },
  PetTraining: {
      screen: PetTraining,
      navigationOptions: {
          header: null
      }
  },
  Petgroomers: {
      screen: Petgroomers,
      navigationOptions: {
          header: null
      }
  },
  PetShelters: {
      screen: PetShelters,
      navigationOptions: {
          header: null
      }
  },
  GiveFeedback: {
      screen: GiveFeedback,
      navigationOptions: {
          header: null
      }
  },
  Edit_pet_detail:{
    screen:Edit_pet_detail,
    navigationOptions:{
      header:null
    }
  },
},{

    initialRouteName: 'SigninScreen',
    // initialRouteName: 'VerificationInfo',

    headerMode: 'none',

});

export default AppNavigator;
