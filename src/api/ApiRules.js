class Constant {
    static BASE_URL = "http://technoarray.com/php/pawh/slim_api/public/";
    static image_path="http://technoarray.com/php/pawh/adopt_images/";
    static image_path2="http://technoarray.com/php/pawh/pet_images/";
    static image_path3="http://technoarray.com/php/pawh/report_images/";
    static image_path4="http://technoarray.com/php/pawh/service_image/"
    static New_BASE="http://technoarray.com/php/pawh/custom/index.php"

    static URL_Signup=Constant.BASE_URL + 'users/signup'
    static URL_Signin= Constant.BASE_URL + 'users/login'
    static URL_Forgot=Constant.BASE_URL + 'users/forgetPassword'
    static URL_requestAdoption=Constant.BASE_URL + 'pets/requestAdoption'
    static URL_saveAdoption=Constant.BASE_URL + 'pets/saveAdoption'
    static URL_changePass=Constant.BASE_URL + 'users/change_password'
    static URL_animalFriendly= Constant.BASE_URL + 'pets/showAnimal'
    static URL_petGroomers=Constant.BASE_URL + 'pets/showPetgroomers'
    static URL_petShelter=Constant.BASE_URL + 'pets/showPetshelters'
    static URL_petTraining=Constant.BASE_URL + 'pets/showPettraining'
    static URL_petVeterinarians=Constant.BASE_URL + 'pets/showVeterinarians'
    static URL_showPetData=Constant.BASE_URL + 'pets/showdata'
    static URL_showDetails=Constant.BASE_URL + 'pets/showadopt'
    static URL_fav=Constant.BASE_URL + 'pets/saveLike'
    static URL_savePet=Constant.BASE_URL + 'pets/addMypet'
    static URL_showMyPet = Constant.BASE_URL + 'pets/showmypet'
    static URL_editMyPet =Constant.BASE_URL +'pets/editMypet'
    static URL_showFav =Constant.BASE_URL + 'pets/showLiked'
    static URL_Stray =Constant.BASE_URL + 'pets/addreport'
    static URL_Unlike=Constant.BASE_URL + 'pets/saveunLike'
    static URL_Feedback=Constant.BASE_URL + 'pets/addfeedback'
    static URL_demo = Constant.BASE_URL + 'test/addtest'
    }

var WebServices = {
    callWebService: function(url, body) {
      var formBody = [];
      for (var property in body) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(body[property]);
        formBody.push(encodedKey + "=" + encodedValue);
      }
      formBody = formBody.join("&");
      //console.log(formBody);
        return fetch(url, {
                method: 'POST',
                headers: {
                    //'Accept': 'application/json',
                    'Accept':'application/json; charset=utf-8',
                    'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8'
                    //'CallToken': body.CallToken ? body.CallToken : '',
                },
                body: formBody
            })
            .then(response => response.text()) // Convert to text instead of res.json()
            .then((text) => {
                return text;
            })
            .then(response => JSON.parse(response)) // Parse the text.
            .then((jsonRes) => {
              //console.log(jsonRes)
                return jsonRes; //main output
            });
    },
    callWebServices: function(url, formBody) {
      //console.log(formBody)

        return fetch(url,{
          method: 'POST',
          headers: {
            'Content-Type': 'multipart/form-data',
          },
          body: formBody
          })
          .then(response => response.json())
          .then((responseJson) => {
              return responseJson; //main output
          })
          .catch(err => {
            console.log(err)
       })
    },

    callWebService_GET: function(url, body) {
        return fetch(url, { // Use your url here
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    //'CallToken': body.CallToken ? body.CallToken : '',
                },
            })
            .then(response => response.text()) // Convert to text instead of res.json()
            .then((text) => {
              return text;
            })
            .then(response => JSON.parse(response)) // Parse the text.
            .then((jsonRes) => {

                // if ((!jsonRes) || (jsonRes.ResponseCode != '200')) {
                //     onError(jsonRes);
                // } else {
                //     onRequestComplete(jsonRes);
                // }

                return jsonRes;
            });
    },

    // json post Data
    callWebService_POST: function(url, body) {
        return fetch(url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    //'CallToken': body.CallToken ? body.CallToken : '',
                },
                body: JSON.stringify(body)
            })
            .then(response => response.text()) // Convert to text instead of res.json()
            .then((text) => {
                return text;
            })
            .then(response => JSON.parse(response)) // Parse the text.
            .then((jsonRes) => {
                return jsonRes; //main output
            });
    }

}
module.exports = {
    Constant,
    WebServices
}
